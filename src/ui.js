// global $,_,dashboard

export function plotlySelect(title, data, callback) {
  const box = dashboard.selectBox(title, data, (d, b) => {
    console.log(d);
    const pbox = $('.plotly', b)[0];
    callback(d, pbox);
  });

  const plotlybox = $('<div></div>');
  plotlybox.addClass('plotly');
  $('.target', box).append(plotlybox);
  const defaultSelection = _.find(data, d => d.selected);
  if (defaultSelection) {
    callback(_.find(data, d => d.selected), plotlybox[0]);
  }

  return box;
}

export function selectDataFromSurvey(formDef) {
  const { items, groups } = formDef;

  const res = [];

  const selectItemFromDef = (def) => {
    const r = {};
    _.assign(r, def);
    r.text = `${r.label} - ${r.id} - ${r.type}`;
    return r;
  };

  _.forEach(items, (def) => {
    if (def.group) {
      const idx = _.findIndex(res, (i) => {
        if (!i.children) {
          return false;
        }
        if (i.text === groups[def.group].label) {
          return true;
        }
        return false;
      });
      if (idx !== -1) {
        res[idx].children.push(selectItemFromDef(def));
      } else {
        const grp = {
          text: groups[def.group].label,
          children: [selectItemFromDef(def)],
        };
        res.push(grp);
      }
    } else {
      // not a group
      res.push(selectItemFromDef(def));
    }
  });

  const defaultItem = _.find(res, (v) => {
    if (_.includes(['select1', 'select', 'number', 'decimal'], v.type)) {
      return true;
    }
    return false;
  });

  if (defaultItem) {
    defaultItem.selected = true;
  }

  return res;
}

export function correlateSelect(title, data, correlate, callback) {
  data.unshift({
    text: 'Select Value',
    id: -1,
  });

  let box = dashboard.selectBox(title, data, (d, b) => {
    console.log(d);
  });

  console.log(box);

  box = dashboard.selectBox(
    title,
    data,
    (d, b) => {
      console.log(d);
    },
    true,
    box,
  );

  const s1 = $('select:eq( 0 )', box).attr('id', 'x-axis');
  const s2 = $('select:eq( 1 )', box).attr('id', 'y-axis');

  const l1 = $('<label>').text('X-Axis / Series Selector');
  l1.attr('for', 'x-axis');
  const l2 = $('<label>').text('Y-Axis');
  l2.attr('for', 'y-axis');

  s1.wrap(l1);
  s2.wrap(l2);

  const button = $('<button>Plot</button>');
  $('.target', box).append(button);

  button.click(() => {
    const sel1 = s1.select2('data');
    const sel2 = s2.select2('data');

    if (sel1.length !== 1 || sel2.length !== 1) {
      return;
    }

    const x = sel1[0];
    const y = sel2[0];

    if (x.type === 'select' || x.type === 'select1') {
      if (y.type === 'select' || y.type === 'select1') {
        // heatmap
        const pbox = $('.plotly', box)[0];

        callback('heatmap', x.id, y.id, pbox);
      } else if (y.type === 'decimal' || y.type === 'int') {
        // averages by order
        const pbox = $('.plotly', box)[0];
        callback('averages', x.id, y.id, pbox);
      }
    } else if (x.type === 'decimal' || x.type === 'int') {
      if (y.type === 'decimal' || y.type === 'int') {
        const pbox = $('.plotly', box)[0];
        callback('scatter', x.id, y.id, pbox);
      }
    }

    console.log('plotting');
    console.log(s1.select2('data'));
    console.log(s2.select2('data'));
  });

  const plotlybox = $('<div></div>');
  plotlybox.addClass('plotly');
  $('.target', box).append(plotlybox);

  if (correlate && correlate.split(',').length === 2) {
    const [colX, colY] = correlate.split(',');
    console.log(colX);
    console.log(colY);
    s1.val(colX);
    s2.val(colY);
    s1.trigger('change');
    s2.trigger('change');
    button.click();
  }

  return box;
}
