/* global dashboard */
/* eslint-disable no-param-reassign */
/// <reference path="../.vscode/globals.d.ts" />
import { init } from 'https://unpkg.com/@oursci/dashboards@1/loader.js';
import * as utils from 'https://unpkg.com/@oursci/dashboards@1/utils.js';

import {
  surveyActivity,
  activityGraph,
  histogram,
  getFormDef,
  barchart,
  waveLengthPlot,
  heatmap,
  scatter,
  hasAllValuesAsNumber,
} from './functions.js';
import { selectDataFromSurvey, plotlySelect, correlateSelect } from './ui.js';
import { sanetizeNumber } from './fixdata.js';

import mapStuff from './mapping.js';

console.log(new URLSearchParams(window.location.search).get('survey'));
const style = {
  primary: '#ef4029',
  secondary: '#26abe3',
  accent: '#26abe3',
};

const currentSurvey = new URLSearchParams(window.location.search).get('survey');
const correlate = new URLSearchParams(window.location.search).get('correlate');
// const results = new URLSearchParams(window.location.search).get('results');

const surveys = {};
if (currentSurvey) {
  surveys.survey = currentSurvey;
}

function postRedner(survey, formDef) {
  console.log(survey);
  const contributors = utils.countDistinct(survey, 'metaUserID');

  dashboard.section('Summary');
  dashboard.info('Surveys Answered', survey.metaInstanceID.length, 'fa-chart-pie');
  dashboard.info('Contributors', _.keys(contributors).length, 'fa-users');
  dashboard.section('Survey as CSV');
  dashboard.csvDownloadBox('Download Survey as CSV', survey);
  dashboard.section('Activity');

  const activity = surveyActivity(survey, 14);
  const activityPlot = activityGraph(activity.activity, activity.measurementActivity);

  dashboard.plotly(
    'Survey Activity',
    activityPlot.data,
    activityPlot.layout,
    activityPlot.config,
    true,
  );

  /*
  dashboard.plotly(
    'Measurement Activity',
    measurementActivityPlot.data,
    measurementActivityPlot.layout,
    measurementActivityPlot.config,
  );
  */

  if (_.find(formDef.items, i => i.type === 'geopoint')) {
    dashboard.section('Map');
    const b = dashboard.genericBox('Map', true);
    mapStuff(survey, formDef, b, style.primary);
  }

  dashboard.section('Results');
  const selectOptions = selectDataFromSurvey(formDef);

  const box = plotlySelect('Value Summary', selectOptions, (data, plotlybox) => {
    switch (data.type) {
      case 'select1':
      case 'select':
        {
          const dataMap = utils.countDistinct(survey, data.id);
          const labels = _.map(
            dataMap,
            (v, k) => _.find(formDef.items, e => e.id === data.id).items[k],
          );
          const p = barchart(dataMap, labels);
          dashboard.plotly(data.label, p.data, p.layout, p.config, true, plotlybox);
        }

        break;
      case 'decimal':
      case 'int':
        {
          sanetizeNumber(survey, data.id);
          const p = histogram(survey[data.id], 50);
          dashboard.plotly(data.label, p.data, p.layout, p.config, true, plotlybox);
        }

        break;
      case 'wavelength':
        console.log('plotting wavelengths');
        waveLengthPlot(
          'Wavelengths',
          'Signal intensity',
          survey,
          _.keys(data.wavelengths),
          plotlybox,
        );

        break;
      default:
        break;
    }

    _.delay(() => {
      plotlybox.scrollIntoView(false);
    }, 200);
  });

  dashboard.section('Correlate');

  const filtered = _.filter(selectOptions, (o) => {
    if (_.includes(['select1', 'select', 'number', 'decimal', 'int'], o.type)) {
      return true;
    }
    return false;
  });
  _.chain(filtered)
    .filter(i => i.selected)
    .forEach((i) => {
      i.selected = false;
    })
    .value();

  correlateSelect(
    'Pick X / Y Axis for correlation',
    filtered,
    correlate,
    (type, colX, colY, plotlybox) => {
      switch (type) {
        case 'heatmap':
          {
            const p = heatmap({
              formDef,
              survey,
              colX,
              colY,
              color: style.primary,
            });

            dashboard.plotly('Heatmap', p.data, p.layout, p.config, true, plotlybox);
          }
          break;
        case 'averages':
          {
            sanetizeNumber(survey, colY);
            const res = {};
            const q = _.find(formDef.items, i => i.id === colX);

            _.forEach(q.items, (option) => {
              const entries = utils.filter(survey, row => row[colX] === option.value);
              let sum = 0;
              let count = 0;
              _.forEach(entries[colY], (e) => {
                if (e) {
                  sum += e;
                  count += 1;
                }
              });
              res[option.value] = sum / count;
            });
            console.log(res);
            const p = barchart(res, _.map(q.items, i => i.label));
            dashboard.plotly('Averages', p.data, p.layout, p.config, true, plotlybox);
          }
          break;
        case 'scatter':
          {
            sanetizeNumber(survey, colX, colY);
            const withNumbers = hasAllValuesAsNumber(survey, [colX, colY]);

            const p = scatter(withNumbers[colX], withNumbers[colY], [
              _.find(formDef.items, i => i.id === colX).label,
              _.find(formDef.items, i => i.id === colY).label,
            ]);
            dashboard.plotly('Correlate', p.data, p.layout, p.config, true, plotlybox);
          }
          break;
        default:
          break;
      }
    },
  );

  console.log(formDef);
}

async function render(surveyData) {
  if (!currentSurvey) {
    dashboard.section('Error');
    dashboard.info('Survey missing', 'No survey passed in URL', 'fa-exclamation-circle');
  } else {
    const { survey } = surveyData;
    const formDef = await getFormDef(currentSurvey, survey);
    postRedner(survey, formDef);
  }
}

init({
  surveys,
  style,
  render,
});
