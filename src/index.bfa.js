import { loadSurvey, surveyUtils } from '@oursci/common';
import _ from 'lodash';
import $ from 'jquery';
import * as Papa from 'papaparse';
import * as Plotly from 'plotly.js/lib/core';
import * as dashboard from './lib/oursci';
import * as utils from './lib/utils';
import * as MathMore from './lib/math';
import { isNumber } from 'util';

export default dashboard; // expose function to the browser window (via oursci variable)

function surveyToCSV(survey) {
  console.log(survey);
  const arr = _.values(survey);
  console.log(_.keys(survey));
  console.log(arr[0]);

  const input = {
    fields: _.keys(survey),
    data: _.zip(...arr),
  };
  console.log(input);
  return Papa.unparse(input);
}

function downloadCsv(data) {
  const payload = surveyToCSV(data);
  const a = document.createElement('a');
  document.body.appendChild(a);
  a.style = 'display: none';
  const blob = new Blob([payload], {
    type: 'text/csv',
  });
  const url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = 'oursci-data.csv';
  a.click();
  window.URL.revokeObjectURL(url);
}

function csvDownloadBox(name, data) {
  const bt = $(
    '<br><span style="cursor: pointer; color: white; background: darkblue; padding: 0.5rem; line-height: 1rem;">Download CSV</span>',
  );
  bt.click(() => {
    downloadCsv(data);
  });
  const id = dashboard.card('', `<div>${name}</div><div class="inner"></div>`, {
    classes: 'col s12 m12',
  });
  $(`#${id} .inner`).append(bt);
}

function mean(array, field) {
  return _.mean(array[field].map(n => Number.parseFloat(n)));
}

function numberFilter(field) {
  return (item) => {
    const v = item[field];
    if (v === undefined) {
      // || v.trim() === '') {
      return false;
    }
    if (Number.isNaN(v)) {
      return false;
    }
    return true;
  };
}

function getSharedN(array1, array2) {
  let totalN = 0;
  for (let i = 0; i < array1.length; i++) {
    //    console.log(Number(array1[i]), Number(array2[i]));
    if (
      array1[i] !== ''
      && !isNaN(Number(array1[i]))
      && array2[i] !== ''
      && !isNaN(Number(array2[i]))
    ) {
      totalN += 1;
    }
  }
  //    console.log(totalN);
  return totalN;
}

function countIfNumber(array1) {
  let totalN = 0;
  for (let i = 0; i < array1.length; i++) {
    if (array1[i] !== '' && !isNaN(Number(array1[i]))) {
      totalN += 1;
    }
  }
  //    console.log(totalN);
  return totalN;
}
// test countIfNumber and getSharedN
/*
  const myArray = [undefined, 1, 2, 3, '', 'joe', 'blah'];
  const myArray2 = [undefined, undefined, 2, 3, '', 'joe', 'blah'];

  console.log('LOOK HERE STUPID');
  console.log(getSharedN(myArray, myArray2));
  console.log(countIfNumber(myArray));
  */

function countIfThere(array) {
  let count = 0;
  for (let i = 0; i < array.length; i++) {
    if (array[i] !== undefined && array[i] !== 'undefined' && array[i] !== '') {
      count += 1;
    }
  }
  return count;
}

function hasAllValuesAsNumber(fullSurvey, requiredIndexes) {
  return utils.filter(fullSurvey, (item) => {
    let valid = true;
    _.forEach(requiredIndexes, (key) => {
      const v = item[key];
      if (v === undefined) {
        // || v.trim() === '') {
        valid = false;
      }
      if (Number.isNaN(v)) {
        valid = false;
      }
    });
    return valid;
  });
}

function tracesFromKeys(validCarrots, carrotscan2Keys) {
  const traces = [];
  _.map(utils.zip(validCarrots), (item) => {
    const yarr = _.map(carrotscan2Keys, (key) => {
      const v = item[key];
      if (v === undefined) {
        // || v.trim() === '') {
        return undefined;
      }
      if (Number.isNaN(v)) {
        return undefined;
      }
      return Number.parseFloat(v);
    });
    traces.push(yarr);
  });
  return traces;
}

function hasAtLeastOneNumber(fullSurvey, keys) {
  return utils.filter(fullSurvey, (item) => {
    let valid = false;
    _.forEach(keys, (value) => {
      const v = item[value];
      if (v === undefined) {
        // || v.trim() === '') {
        return;
      }
      if (Number.isNaN(item[value])) {
        return;
      }
      valid = true;
    });
    return valid;
  });
}

function stateMap(title, stateSampleCount, hoverTexts, scale, center) {
  const data = [
    {
      type: 'choropleth',
      locationmode: 'USA-states',
      locations: _.keys(stateSampleCount),
      z: _.values(stateSampleCount),
      text: hoverTexts,
      zmin: 0,
      zmax: Math.max(..._.values(stateSampleCount)),
      colorscale: [
        [0, 'rgb(242,240,247)'],
        [0.2, 'rgb(218,218,235)'],
        [0.4, 'rgb(188,189,220)'],
        [0.6, 'rgb(158,154,200)'],
        [0.8, 'rgb(117,107,177)'],
        [1, 'rgb(84,39,143)'],
      ],
      colorbar: {
        title: '# Surveyed',
        thickness: 0.2,
      },
      marker: {
        line: {
          color: 'rgb(255,255,255)',
          width: 2,
        },
      },
    },
  ];

  const layout = {
    title,
    geo: {
      scope: 'usa',
      showlakes: true,
      lakecolor: 'rgb(255,255,255)',
      projection: {
        scale,
      },
      center,
    },
  };

  const id = dashboard.card('', '<div class="statemap"></div>', {
    classes: 'col s12 m12',
  });

  const div = document.getElementById(id).getElementsByClassName('statemap')[0];
  Plotly.plot(div, data, layout, {
    showLink: false,
  });
}

function barchart(title, x, values) {
  const xshouldbe = ['polyphenols', 'antioxidants', 'proteins'];
  const demo = {
    organic: [1, 2, 3],
  };

  const traces = [];
  _.forEach(values, (y, name) => {
    console.log(name);
    traces.push({
      x,
      y,
      name,
      type: 'bar',
    });
  });

  const layout = {
    barmode: 'group',
    title,
  };

  const id = dashboard.card('', '<div class="barchart"></div>', {
    classes: 'col s12 m12',
  });

  const div = document.getElementById(id).getElementsByClassName('barchart')[0];
  Plotly.plot(div, traces, layout);
}

function linecharts(title, x, values, axis) {
  const traces = _.map(values, v => ({
    x,
    y: v,
    type: 'scatter',
    connectgaps: true,
    mode: 'lines',
    line: {
      width: 0.2,
      color: '#3390E6',
      smoothing: 1,
      shape: 'spline',
      simplify: true,
    },
  }));

  const layout = {
    title,
    xaxis: {
      title: axis[0],
    },
    yaxis: {
      title: axis[1],
    },
  };

  const id = dashboard.card('', '<div class="linechart"></div>', {
    classes: 'col s12 m12',
  });

  const div = document.getElementById(id).getElementsByClassName('linechart')[0];
  Plotly.plot(div, traces, layout);
}

function scatter(title, x, y, axis) {
  const trace = {
    x,
    y,
    mode: 'markers',
    type: 'scatter',
  };

  const layout = {
    title,
    xaxis: {
      title: axis[0],
    },
    yaxis: {
      title: axis[1],
    },
  };

  const id = dashboard.card('', '<div class="linechart"></div>', {
    classes: 'col s12 m12',
  });

  const div = document.getElementById(id).getElementsByClassName('linechart')[0];
  Plotly.plot(div, [trace], layout);
}

(async () => {
  await dashboard.init(); // init the dashboard library, must always be called first

  dashboard.showLoading(true); // show the loading screen

  // ////////////////////////////////// LOAD DATA ///////////////////////////////////////////

  const surveys = await loadSurvey(
    'build_RFC-Sample-Collection-Survey_1529938938', // move store_group.store_additional into farm_practice keep existing values
    'build_RFC-Sample-Collection-Survey-v2_1531946011',
    'build_Sample-Collection-Survey-RFC_1531751577',
    'build_Sample-Collection-Survey-RFC-v3_1532958979',
  );

  const allSoilQuality = await loadSurvey(
    'build_Soil-Quality-Survey-v2_1533147719',
    'build_Soil-Quality-Survey-v3_1536173868',
  );

  const allCarrot = await loadSurvey(
    'build_Carrot-Quality-Survey-A2_1531937323', // carrot scan raw + supernatant only, shift wavelengths. delete all else
    'build_Carrot-Quality-Survey-A4_1532465603', // carrot scan raw + supernatant only, shift wavelengths.  delete all else
    'build_Carrot-Quality-Survey-A5_1533131731', // shift wavelengths, note that supernatant was done on correct wavelengths but carrotscan needs to be shifted.
    'build_Carrot-Quality-Survey-A6_1533563832',
    'build_Carrot-Quality-Survey-A7_1534367514',
    'build_Carrot-Quality-Survey-v1_1536173572',
  );

  const allSpinach = await loadSurvey(
    'build_Spinach-Quality-Survey-A1_1531938554', // leaf scan data only, shift wavelengths. delete all else
    'build_Spinach-Quality-Survey-A2_1532541479', // supernatant only, shift wavelengths.  delete all else
    'build_Spinach-Quality-Survey-A3_1533131824', // no leaf scan data, use supernatant, shift wavelengths.
    'build_Spinach-Quality-Survey-A4_1534367478',
    'build_Spinach-Quality-Survey-v2_1536173235',
  );

  // ////////////////////////////////// CLEAN DATA ///////////////////////////////////////////
  // Make some lists of things we know we need to clean up
  // lists are different for different surveys based on comments above
  // in some cases we delete columns, in some we change names (fix wavelength naming mistake), sometimes both.

  const wavelengthRename = {
    median_370: 'median_365',
    median_395: 'median_385',
    median_420: 'median_450',
    median_530: 'median_500',
    median_605: 'median_530',
    median_650: 'median_587',
    median_730: 'median_632',
    spad_370: 'spad_365',
    spad_395: 'spad_385',
    spad_420: 'spad_450',
    spad_530: 'spad_500',
    spad_605: 'spad_530',
    spad_650: 'spad_587',
    spad_730: 'spad_632',
    three_stdev_370: 'three_stdev_365',
    three_stdev_395: 'three_stdev_385',
    three_stdev_420: 'three_stdev_450',
    three_stdev_530: 'three_stdev_500',
    three_stdev_605: 'three_stdev_530',
    three_stdev_650: 'three_stdev_587',
    three_stdev_730: 'three_stdev_632',
    'Total Polyphenols mg GAE 100g FW': 'polyphenolsMgGae100gFw',
    'Total Protein mg per 100g FW': 'proteinMgPer100g',
    'Total Antioxidants FRAP value': 'antioxidentsFrap',
  };

  const otherRename = {
    // only used for carrot A6
    'Total Polyphenols mg GAE 100g FW': 'polyphenolsMgGae100gFw',
    'Total Protein mg per 100g FW': 'proteinMgPer100g',
    'Total Antioxidants FRAP value': 'antioxidentsFrap',
  };

  let thisSurvey; // temporary reference for survey being modified
  let surveyName = ''; // temporary reference for survey being modified

  // an early survey had the farm practice question under a different name, fixing that here.
  surveyName = 'build_RFC-Sample-Collection-Survey_1529938938';
  const thisQuestion = surveys[surveyName]['store_group.store_additional'];
  const thatQuestion = surveys[surveyName].farm_practice;
  for (let i = 0; i < thisQuestion.length; i++) {
    if (thisQuestion[i] !== '') {
      thatQuestion[i] = _.clone(thisQuestion[i], true);
      //      console.log(`replace ${thatQuestion[i]} with ${thisQuestion[i]}`);
    }
  }

  const carrotKeepList = [
    'sample_id',
    'carrot_color',
    'carrot_diameter',
    'metaDateCreated',
    'metaDateModified',
    'metaInstanceID',
    'metaUserID',
    'sample_quality',
    'carrotscan1.data.median_370',
    'carrotscan1.data.median_395',
    'carrotscan1.data.median_420',
    'carrotscan1.data.median_530',
    'carrotscan1.data.median_605',
    'carrotscan1.data.median_650',
    'carrotscan1.data.median_730',
    'carrotscan1.data.median_850',
    'carrotscan1.data.median_880',
    'carrotscan1.data.median_940',
    'carrotscan1.data.median_365', //
    'carrotscan1.data.median_385', //
    'carrotscan1.data.median_450', //
    'carrotscan1.data.median_500', //
    'carrotscan1.data.median_587', //
    'carrotscan1.data.median_632', //
    'carrotscan2.data.median_370',
    'carrotscan2.data.median_395',
    'carrotscan2.data.median_420',
    'carrotscan2.data.median_530',
    'carrotscan2.data.median_605',
    'carrotscan2.data.median_650',
    'carrotscan2.data.median_730',
    'carrotscan2.data.median_850',
    'carrotscan2.data.median_880',
    'carrotscan2.data.median_940',
    'carrotscan2.data.median_365', //
    'carrotscan2.data.median_385', //
    'carrotscan2.data.median_450', //
    'carrotscan2.data.median_500', //
    'carrotscan2.data.median_587', //
    'carrotscan2.data.median_632', //
    'supernatant.data.median_370',
    'supernatant.data.median_395',
    'supernatant.data.median_420',
    'supernatant.data.median_530',
    'supernatant.data.median_605',
    'supernatant.data.median_650',
    'supernatant.data.median_730',
    'supernatant.data.median_850',
    'supernatant.data.median_880',
    'supernatant.data.median_940',
    'supernatant.data.median_365', //
    'supernatant.data.median_385', //
    'supernatant.data.median_450', //
    'supernatant.data.median_500', //
    'supernatant.data.median_587', //
    'supernatant.data.median_632', //
  ];

  // delete parts of surveys which were identified as bad, replace mistaken values for wavelength.
  surveyName = 'build_Carrot-Quality-Survey-A2_1531937323';
  thisSurvey = allCarrot[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (!_.includes(carrotKeepList, k)) {
      delete thisSurvey[k];
    } else if (_.includes(_.keys(wavelengthRename), toReplace)) {
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  // had to mark all replacements with _n (otherwise they can get double checked and incorrectly removed), so now go back and remove the _n
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });

  //
  surveyName = 'build_Carrot-Quality-Survey-A4_1532465603';
  thisSurvey = allCarrot[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (_.includes(_.keys(wavelengthRename), toReplace)) {
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });
  surveyName = 'build_Carrot-Quality-Survey-A5_1533131731';
  thisSurvey = allCarrot[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (_.includes(_.keys(wavelengthRename), toReplace) && !_.includes(k, 'supernatant')) {
      // Supernatant was run with the new reflectometer + wavelengths, so skip
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });
  surveyName = 'build_Carrot-Quality-Survey-A6_1533563832';
  thisSurvey = allCarrot[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (_.includes(_.keys(otherRename), toReplace)) {
      // Supernatant was run with the new reflectometer + wavelengths, so skip
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${otherRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, otherRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });

  const keepSpinachLeafList = [
    'sample_id',
    'metaDateCreated',
    'metaDateModified',
    'metaInstanceID',
    'metaUserID',
    'sample_quality',
    'leafscan1.data.median_370',
    'leafscan1.data.median_395',
    'leafscan1.data.median_420',
    'leafscan1.data.median_530',
    'leafscan1.data.median_605',
    'leafscan1.data.median_650',
    'leafscan1.data.median_730',
    'leafscan1.data.median_850',
    'leafscan1.data.median_880',
    'leafscan1.data.median_940',
    'leafscan1.data.median_365', //
    'leafscan1.data.median_385', //
    'leafscan1.data.median_450', //
    'leafscan1.data.median_500', //
    'leafscan1.data.median_587', //
    'leafscan1.data.median_632', //
    'leafscan2.data.median_370',
    'leafscan2.data.median_395',
    'leafscan2.data.median_420',
    'leafscan2.data.median_530',
    'leafscan2.data.median_605',
    'leafscan2.data.median_650',
    'leafscan2.data.median_730',
    'leafscan2.data.median_850',
    'leafscan2.data.median_880',
    'leafscan2.data.median_940',
    'leafscan2.data.median_365', //
    'leafscan2.data.median_385', //
    'leafscan2.data.median_450', //
    'leafscan2.data.median_500', //
    'leafscan2.data.median_587', //
    'leafscan2.data.median_632', //
    'leafscan3.data.median_370',
    'leafscan3.data.median_395',
    'leafscan3.data.median_420',
    'leafscan3.data.median_530',
    'leafscan3.data.median_605',
    'leafscan3.data.median_650',
    'leafscan3.data.median_730',
    'leafscan3.data.median_850',
    'leafscan3.data.median_880',
    'leafscan3.data.median_940',
    'leafscan3.data.median_365', //
    'leafscan3.data.median_385', //
    'leafscan3.data.median_450', //
    'leafscan3.data.median_500', //
    'leafscan3.data.median_587', //
    'leafscan3.data.median_632', //
  ];

  // delete parts of surveys which were identified as bad
  surveyName = 'build_Spinach-Quality-Survey-A1_1531938554';
  thisSurvey = allSpinach[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (!_.includes(keepSpinachLeafList, k)) {
      delete thisSurvey[k];
    } else if (_.includes(_.keys(wavelengthRename), toReplace)) {
      // console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      // console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });

  const keepSpinachSuperList = [
    'sample_id',
    'metaDateCreated',
    'metaDateModified',
    'metaInstanceID',
    'metaUserID',
    'sample_quality',
    'supernatant.data.median_370',
    'supernatant.data.median_395',
    'supernatant.data.median_420',
    'supernatant.data.median_530',
    'supernatant.data.median_605',
    'supernatant.data.median_650',
    'supernatant.data.median_730',
    'supernatant.data.median_850',
    'supernatant.data.median_880',
    'supernatant.data.median_940',
    'supernatant.data.median_365', //
    'supernatant.data.median_385', //
    'supernatant.data.median_450', //
    'supernatant.data.median_500', //
    'supernatant.data.median_587', //
    'supernatant.data.median_632', //
  ];

  surveyName = 'build_Spinach-Quality-Survey-A2_1532541479';
  thisSurvey = allSpinach[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (!_.includes(keepSpinachSuperList, k)) {
      delete thisSurvey[k];
    } else if (_.includes(_.keys(wavelengthRename), toReplace)) {
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });
  surveyName = 'build_Spinach-Quality-Survey-A3_1533131824';
  thisSurvey = allSpinach[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (_.includes(_.keys(wavelengthRename), toReplace)) {
      //      console.log(`in survey ${surveyName}\n replaced this ${toReplace} with this: ${wavelengthRename[toReplace]}`);
      thisSurvey[`${_.replace(k, toReplace, wavelengthRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });
  surveyName = 'build_Spinach-Quality-Survey-A4_1534367478';
  thisSurvey = allSpinach[surveyName];
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('.').slice(-1)[0];
    if (_.includes(_.keys(otherRename), toReplace)) {
      console.log(
        `in survey ${surveyName}\n replaced this ${toReplace} with this: ${otherRename[toReplace]}`,
      );
      thisSurvey[`${_.replace(k, toReplace, otherRename[toReplace])}_n`] = _.cloneDeep(
        thisSurvey[k],
        true,
      );
      delete thisSurvey[k];
    }
  });
  _.forEach(_.keys(thisSurvey), (k) => {
    const toReplace = k.split('_').slice(-1)[0];
    if (toReplace === 'n') {
      //      console.log(`in survey ${surveyName}/n created new column ${k.slice(0, -2)}`);
      thisSurvey[k.slice(0, -2)] = _.cloneDeep(thisSurvey[k], true);
      delete thisSurvey[k];
    }
  });

  /*
    console.log('surveys merged');
    console.log(surveys);

    console.log('carrots merged');
    console.log(allCarrot);

    console.log('spinach merged');
    console.log(allSpinach);

    console.log('soil merged');
    console.log(allSoilQuality);
      */

  const merged = surveyUtils.mergeSurveys(surveys);
  const soilQuality = surveyUtils.mergeSurveys(allSoilQuality);
  const carrots = surveyUtils.mergeSurveys(allCarrot);
  const spinach = surveyUtils.mergeSurveys(allSpinach);

  /*
    csvDownloadBox('surveys', merged);
    csvDownloadBox('soil', soilQuality);
    csvDownloadBox('carrots', carrots);
    csvDownloadBox('spinach', spinach);
    */

  // set outliers for soil carbon and respiration to undefined
  for (let i = 0; i < soilQuality.metaInstanceID.length; i++) {
    if (
      (soilQuality['display_respiration.data.Max_co2'][i] !== ''
        && (Number(soilQuality['display_respiration.data.Max_co2'][i]) > 10000
          || Number(soilQuality['display_respiration.data.Min_co2'][i]) > 10000
          || Number(soilQuality['display_respiration.data.ugc_gsoil'][i]) <= 0))
      || Number(soilQuality['display_respiration.data.co2_increase'][i]) > 10000
    ) {
      console.log(
        `set undefined from ${soilQuality['display_respiration.data.ugc_gsoil'][i]}, high ${
          soilQuality['display_respiration.data.Max_co2'][i]
        }, low ${soilQuality['display_respiration.data.Min_co2'][i]}.`,
      );
      //      console.log(soilQuality['display_respiration.data.ugc_gsoil'][i]);
      soilQuality['display_respiration.data.ugc_gsoil'][i] = undefined;
    }
    if (Number(soilQuality['display_loi.data.Total C'][i]) <= 0) {
      console.log(`set undefined from ${soilQuality['display_loi.data.Total C'][i]}`);
      //      console.log(soilQuality['display_respiration.data.ugc_gsoil'][i]);
      soilQuality['display_loi.data.Total C'][i] = undefined;
    }
  }

  // create separate surveys for 0 - 15cm (3) and 15 - 30cm (6)
  const soilQuality3 = utils.filter(soilQuality, (item) => {
    if (item.Depth === '3') {
      return true;
    }
    return false;
  });
  //  console.log('soil quality 3');
  //  console.log(soilQuality3);
  const soilQuality9 = utils.filter(soilQuality, (item) => {
    if (item.Depth === '9') {
      return true;
    }
    return false;
  });
  //  console.log('soil quality 9');
  //  console.log(soilQuality9);

  // DO FINAL JOIN OF CLEANED SURVEYS
  const joinedWithSoil3 = surveyUtils.joinSurveyOn(
    merged,
    'Sample_ID',
    soilQuality3,
    'sample_id',
    'soil3',
  );

  const joinedWithSoil9 = surveyUtils.joinSurveyOn(
    joinedWithSoil3,
    'Sample_ID',
    soilQuality9,
    'sample_id',
    'soil9',
  );

  const joinedWithCarrots = surveyUtils.joinSurveyOn(
    joinedWithSoil9,
    'Sample_ID',
    carrots,
    'sample_id',
    'carrots',
  );

  const fullSurvey = surveyUtils.joinSurveyOn(
    joinedWithCarrots,
    'Sample_ID',
    spinach,
    'sample_id',
    'spinach',
  );

  // establish keys that we're going to be analyzing so we can clean them up (ensure they are numbers or undefined)

  const carrotKeys = {
    Polyphenols: 'carrots.veg_results.data.polyphenolsMgGae100gFw',
    Antioxidants: 'carrots.veg_results.data.antioxidentsFrap',
    Proteins: 'carrots.veg_results.data.proteinMgPer100g',
  };
  const spinachKeys = {
    Polyphenols: 'spinach.veg_results.data.polyphenolsMgGae100gFw',
    Antioxidants: 'spinach.veg_results.data.antioxidentsFrap',
    Proteins: 'spinach.veg_results.data.proteinMgPer100g',
  };
  const soilKeys3 = {
    'Total Carbon': 'soil3.display_loi.data.Total C',
    Respiration: 'soil3.display_respiration.data.ugc_gsoil',
  };

  const soilKeys9 = {
    'Total Carbon': 'soil9.display_loi.data.Total C',
    Respiration: 'soil9.display_respiration.data.ugc_gsoil',
  };

  const soil3ScanVisKeys = {
    365: 'soil3.scan_vis.data.median_365',
    385: 'soil3.scan_vis.data.median_385',
    450: 'soil3.scan_vis.data.median_450',
    500: 'soil3.scan_vis.data.median_500',
    530: 'soil3.scan_vis.data.median_530',
    587: 'soil3.scan_vis.data.median_587',
    632: 'soil3.scan_vis.data.median_632',
    850: 'soil3.scan_vis.data.median_850',
    880: 'soil3.scan_vis.data.median_880',
    940: 'soil3.scan_vis.data.median_940',
  };

  const soil9ScanVisKeys = {
    365: 'soil9.scan_vis.data.median_365',
    385: 'soil9.scan_vis.data.median_385',
    450: 'soil9.scan_vis.data.median_450',
    500: 'soil9.scan_vis.data.median_500',
    530: 'soil9.scan_vis.data.median_530',
    587: 'soil9.scan_vis.data.median_587',
    632: 'soil9.scan_vis.data.median_632',
    850: 'soil9.scan_vis.data.median_850',
    880: 'soil9.scan_vis.data.median_880',
    940: 'soil9.scan_vis.data.median_940',
  };

  // create arrays so we can run through all the scans and do stuff to them (make numbers, average etc.)
  const carrotscan2Keys = {
    365: 'carrots.carrotscan2.data.median_365',
    385: 'carrots.carrotscan2.data.median_385',
    450: 'carrots.carrotscan2.data.median_450',
    500: 'carrots.carrotscan2.data.median_500',
    530: 'carrots.carrotscan2.data.median_530',
    587: 'carrots.carrotscan2.data.median_587',
    632: 'carrots.carrotscan2.data.median_632',
    850: 'carrots.carrotscan2.data.median_850',
    880: 'carrots.carrotscan2.data.median_880',
    940: 'carrots.carrotscan2.data.median_940',
  };

  const carrotscan1Keys = {
    365: 'carrots.carrotscan1.data.median_365',
    385: 'carrots.carrotscan1.data.median_385',
    450: 'carrots.carrotscan1.data.median_450',
    500: 'carrots.carrotscan1.data.median_500',
    530: 'carrots.carrotscan1.data.median_530',
    587: 'carrots.carrotscan1.data.median_587',
    632: 'carrots.carrotscan1.data.median_632',
    850: 'carrots.carrotscan1.data.median_850',
    880: 'carrots.carrotscan1.data.median_880',
    940: 'carrots.carrotscan1.data.median_940',
  };

  const carrotscanAverageKeys = {
    365: 'carrotscan.median_365',
    385: 'carrotscan.median_385',
    450: 'carrotscan.median_450',
    500: 'carrotscan.median_500',
    530: 'carrotscan.median_530',
    587: 'carrotscan.median_587',
    632: 'carrotscan.median_632',
    850: 'carrotscan.median_850',
    880: 'carrotscan.median_880',
    940: 'carrotscan.median_940',
  };

  const carrotsSuperNatantKeys = {
    365: 'carrots.supernatant.data.median_365',
    385: 'carrots.supernatant.data.median_385',
    450: 'carrots.supernatant.data.median_450',
    500: 'carrots.supernatant.data.median_500',
    530: 'carrots.supernatant.data.median_530',
    587: 'carrots.supernatant.data.median_587',
    632: 'carrots.supernatant.data.median_632',
    850: 'carrots.supernatant.data.median_850',
    880: 'carrots.supernatant.data.median_880',
    940: 'carrots.supernatant.data.median_940',
  };

  const spinachscan1Keys = {
    365: 'spinach.leafscan1.data.median_365',
    385: 'spinach.leafscan1.data.median_385',
    450: 'spinach.leafscan1.data.median_450',
    500: 'spinach.leafscan1.data.median_500',
    530: 'spinach.leafscan1.data.median_530',
    587: 'spinach.leafscan1.data.median_587',
    632: 'spinach.leafscan1.data.median_632',
    850: 'spinach.leafscan1.data.median_850',
    880: 'spinach.leafscan1.data.median_880',
    940: 'spinach.leafscan1.data.median_940',
  };

  const spinachscan2Keys = {
    365: 'spinach.leafscan2.data.median_365',
    385: 'spinach.leafscan2.data.median_385',
    450: 'spinach.leafscan2.data.median_450',
    500: 'spinach.leafscan2.data.median_500',
    530: 'spinach.leafscan2.data.median_530',
    587: 'spinach.leafscan2.data.median_587',
    632: 'spinach.leafscan2.data.median_632',
    850: 'spinach.leafscan2.data.median_850',
    880: 'spinach.leafscan2.data.median_880',
    940: 'spinach.leafscan2.data.median_940',
  };

  const spinachscan3Keys = {
    365: 'spinach.leafscan3.data.median_365',
    385: 'spinach.leafscan3.data.median_385',
    450: 'spinach.leafscan3.data.median_450',
    500: 'spinach.leafscan3.data.median_500',
    530: 'spinach.leafscan3.data.median_530',
    587: 'spinach.leafscan3.data.median_587',
    632: 'spinach.leafscan3.data.median_632',
    850: 'spinach.leafscan3.data.median_850',
    880: 'spinach.leafscan3.data.median_880',
    940: 'spinach.leafscan3.data.median_940',
  };

  const spinachscanAverageKeys = {
    365: 'spinachscan.median_365',
    385: 'spinachscan.median_385',
    450: 'spinachscan.median_450',
    500: 'spinachscan.median_500',
    530: 'spinachscan.median_530',
    587: 'spinachscan.median_587',
    632: 'spinachscan.median_632',
    850: 'spinachscan.median_850',
    880: 'spinachscan.median_880',
    940: 'spinachscan.median_940',
  };

  const spinachSuperNatantKeys = {
    365: 'spinach.supernatant.data.median_365',
    385: 'spinach.supernatant.data.median_385',
    450: 'spinach.supernatant.data.median_450',
    500: 'spinach.supernatant.data.median_500',
    530: 'spinach.supernatant.data.median_530',
    587: 'spinach.supernatant.data.median_587',
    632: 'spinach.supernatant.data.median_632',
    850: 'spinach.supernatant.data.median_850',
    880: 'spinach.supernatant.data.median_880',
    940: 'spinach.supernatant.data.median_940',
  };

  // make sure everything you want to analyze that needs to be a number, is a number or is undefined
  MathMore.makeNumberOrUndefined(fullSurvey[carrotKeys.Polyphenols]);
  MathMore.makeNumberOrUndefined(fullSurvey[carrotKeys.Antioxidants]);
  MathMore.makeNumberOrUndefined(fullSurvey[carrotKeys.Proteins]);
  MathMore.makeNumberOrUndefined(fullSurvey[spinachKeys.Polyphenols]);
  MathMore.makeNumberOrUndefined(fullSurvey[spinachKeys.Antioxidants]);
  MathMore.makeNumberOrUndefined(fullSurvey[spinachKeys.Proteins]);
  MathMore.makeNumberOrUndefined(fullSurvey[soilKeys3['Total Carbon']]);
  MathMore.makeNumberOrUndefined(fullSurvey[soilKeys3.Respiration]);
  MathMore.makeNumberOrUndefined(fullSurvey[soilKeys9['Total Carbon']]);
  MathMore.makeNumberOrUndefined(fullSurvey[soilKeys9.Respiration]);

  // average and make numbers only scans for carrots
  _.forEach(_.keys(carrotscanAverageKeys), (k) => {
    MathMore.makeNumberOrUndefined(fullSurvey[carrotsSuperNatantKeys[k]]);
    MathMore.makeNumberOrUndefined(fullSurvey[carrotscan1Keys[k]]);
    MathMore.makeNumberOrUndefined(fullSurvey[carrotscan2Keys[k]]);
    fullSurvey[carrotscanAverageKeys[k]] = _.map(fullSurvey[carrotscan1Keys[k]], (item, index) => {
      let averaged;
      if (item !== undefined && fullSurvey[carrotscan2Keys[k]][index] !== undefined) {
        averaged = (item + fullSurvey[carrotscan2Keys[k]][index]) / 2;
      }
      return averaged;
    });
  });

  // average and make numbers only scans for spinach
  _.forEach(_.keys(spinachscanAverageKeys), (k) => {
    MathMore.makeNumberOrUndefined(fullSurvey[spinachSuperNatantKeys[k]]);
    MathMore.makeNumberOrUndefined(fullSurvey[spinachscan1Keys[k]]);
    MathMore.makeNumberOrUndefined(fullSurvey[spinachscan2Keys[k]]);
    MathMore.makeNumberOrUndefined(fullSurvey[spinachscan3Keys[k]]);
    fullSurvey[spinachscanAverageKeys[k]] = _.map(
      fullSurvey[spinachscan1Keys[k]],
      (item, index) => {
        let averaged;
        if (
          item !== undefined
          && fullSurvey[spinachscan2Keys[k]][index] !== undefined
          && fullSurvey[spinachscan3Keys[k]][index] !== undefined
        ) {
          averaged = (item
              + fullSurvey[spinachscan2Keys[k]][index]
              + fullSurvey[spinachscan3Keys[k]][index])
            / 3;
        }
        return averaged;
      },
    );
  });

  //  console.log('FULL MERGED / CLEANED SURVEY!!!');
  csvDownloadBox('Download combined survey (soil, food, metadata) as csv', fullSurvey);

  // ////////////////////////////////// PRODUCE GENERAL DESCRIPTIVES ///////////////////////////////////////////

  // minerals for food in csv  ['Na','Mg','Al','Si','P','S','Cl','Rh','K','Ca','Mn','Fe','Ni','Cu','Zn','Ba','Cr','Co','As','Pb','Se','Mo'] - 22 total
  // minerals for soil in csv ['Na','Mg','Al','Si','P','S','Rh','K','Ca','Ba','Ti','V','Cr','Mn','Fe','Ni','Cu','Zn','Pb','Rb','Sr','Co','As','Se','Mo'] - 25 total
  // sometimes one or more minerals may be missing.
  const foodMinerals = [
    'Na',
    'Mg',
    'Al',
    'Si',
    'P',
    'S',
    'Cl',
    'Rh',
    'K',
    'Ca',
    'Mn',
    'Fe',
    'Ni',
    'Cu',
    'Zn',
    'Ba',
    'Cr',
    'Co',
    'As',
    'Pb',
    'Se',
    'Mo',
  ];
  const soilMinerals = [
    'Na',
    'Mg',
    'Al',
    'Si',
    'P',
    'S',
    'Rh',
    'K',
    'Ca',
    'Ba',
    'Ti',
    'V',
    'Cr',
    'Mn',
    'Fe',
    'Ni',
    'Cu',
    'Zn',
    'Pb',
    'Rb',
    'Sr',
    'Co',
    'As',
    'Se',
    'Mo',
  ];
  const sharedMinerals = [
    'Na',
    'Mg',
    'Al',
    'Si',
    'P',
    'S',
    'Cl',
    'Rh',
    'K',
    'Ca',
    'Mn',
    'Fe',
    'Ni',
    'Cu',
    'Zn',
    'Ba',
    'Cr',
    'As',
    'Pb',
    'Se',
    'Mo',
  ];

  /*
    function shared(item) {
      _.find(foodMinerals, item);
    }
    */

  /*
    const sharedMinerals = [];
    _.forEach(soilMinerals, (item) => {
      console.log(item);
      console.log(_.find(foodMinerals, item));
      if (_.find(foodMinerals, item)) {
        sharedMinerals.push(item);
      }
    });

    console.log(sharedMinerals);
  */

  // create new categories for minerals, populate with undefined for whole survey
  _.forEach(foodMinerals, (item) => {
    fullSurvey[`food_${item}`] = [];
    _.forEach(fullSurvey.metaInstanceID, (id, index) => {
      fullSurvey[`food_${item}`][index] = undefined;
    });
  });
  const soil3 = 'soil_0_15_';
  const soil9 = 'soil_15_30_';
  _.forEach(soilMinerals, (item) => {
    fullSurvey[`${soil3}${item}`] = [];
    fullSurvey[`${soil9}${item}`] = [];
    _.forEach(fullSurvey.metaInstanceID, (id, index) => {
      fullSurvey[`${soil3}${item}`][index] = undefined;
      fullSurvey[`${soil9}${item}`][index] = undefined;
    });
  });

  // break out the minerals from array to individual variables for soils
  _.forEach(fullSurvey['soil.xrf.data.elements'], (item, index) => {
    if (item) {
      const elements = item.split(',');
      //      console.log(elements);
      _.forEach(elements, (element, index2) => {
        if (fullSurvey['soil.xrf.data.elements_ppm'][index]) {
          if (!isNaN(Number(fullSurvey['soil.xrf.data.elements_ppm'][index].split(',')[index2]))) {
            //            console.log(`${soil3}${element}`);
            //            console.log(`${soil9}${element}`);
            //            console.log(fullSurvey['soil.xrf.data.elements_ppm'][index]);
            if (fullSurvey['soil.Depth'][index] === '3') {
              fullSurvey[`${soil3}${element.trim()}`][index] = fullSurvey[
                'soil.xrf.data.elements_ppm'
              ][index].split(',')[index2];
            } else if (fullSurvey['soil.Depth'][index] === '9') {
              fullSurvey[`${soil9}${element.trim()}`][index] = fullSurvey[
                'soil.xrf.data.elements_ppm'
              ][index].split(',')[index2];
            }
          }
        }
      });
    }
  });

  // break out the minerals from array to individual variables for carrots
  _.forEach(fullSurvey['carrots.xrf.data.elements'], (item, index) => {
    if (item) {
      const elements = item.split(',');
      //      console.log(elements);
      //      console.log(fullSurvey['carrots.xrf.data.elements_ppm'][index]);
      _.forEach(elements, (element, index2) => {
        if (fullSurvey['carrots.xrf.data.elements_ppm'][index]) {
          if (
            !isNaN(Number(fullSurvey['carrots.xrf.data.elements_ppm'][index].split(',')[index2]))
          ) {
            //            console.log(fullSurvey['carrots.xrf.data.elements_ppm'][index]);
            fullSurvey[`food_${element.trim()}`][index] = fullSurvey[
              'carrots.xrf.data.elements_ppm'
            ][index].split(',')[index2];
          }
        }
      });
    }
  });

  console.log('full survey after creating individualized element variables');
  console.log(fullSurvey);

  const carrotsOnly = utils.filter(fullSurvey, (item) => {
    if (item.sample_type === 'carrot') {
      return true;
    }
    return false;
  });
  const spinachOnly = utils.filter(fullSurvey, (item) => {
    if (item.sample_type === 'spinach') {
      return true;
    }
    return false;
  });

  /*
    const foodMinerals = ['Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Rh', 'K', 'Ca', 'Mn', 'Fe', 'Ni', 'Cu', 'Zn', 'Ba', 'Cr', 'Co', 'As', 'Pb', 'Se', 'Mo'];
    const soilMinerals = ['Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Rh', 'K', 'Ca', 'Ba', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Ni', 'Cu', 'Zn', 'Pb', 'Rb', 'Sr', 'Co', 'As', 'Se', 'Mo'];
    */

  dashboard.table(
    {
      'Total Organic Carbon': '% carbon in soil (dried @ 105C)',
      'Soil Respiration': 'ug mineralizable carbon per gram soil (air dried)',
      Antioxidants: 'uM Gallic Acid equivalents (FRAP method), dry weight',
      Polyphenols: 'ug/ml Gallic Acid equivalents (Folin Ciocalteu method), dry weight',
      Proteins: 'mg protein per 100g dry weight',
    },
    'Units for measurements referenced below',
  );

  dashboard.table(
    {
      "'biological'":
        'each of the following items gives +1 to biological score: certified organic, organic, uses biological amendments, non-chlroinated irrigation water, no till, uses cover crops, no spray, biodynamic',
      Irrigation: "'frequently used irrigation' checked",
      noBio: "nothing in 'biological' list checked",
      someBio: "1 - 2 'biological' checked",
      lotsBio: "3 or more 'biological' checked",
      organic: "'certified organic' checked",
      organicNotCert: "'organic' checked, 'certified organic' not checked",
    },
    'Description of Categories referenced below',
  );

  // show descriptives for relevant variables
  const showList = {
    'Soil Respiration 0 - 15cm': fullSurvey[soilKeys3.Respiration],
    'Total Organic Carbon 0 - 15cm': fullSurvey[soilKeys3['Total Carbon']],
    'Soil Respiration 15 - 30cm': fullSurvey[soilKeys9.Respiration],
    'Total Organic Carbon 15 - 30cm': fullSurvey[soilKeys9['Total Carbon']],
    /*
      'Soil 0-15cm Phospohorus': fullSurvey['soil_0_15_P'],
      'Soil 0-15cm Calcium': fullSurvey['soil_0_15_Ca'],
      'Soil 0-15cm Copper': fullSurvey['soil_0_15_Cu'],
      'Soil 0-15cm Magnesium': fullSurvey['soil_0_15_Mg'],
      'Soil 0-15cm Manganese': fullSurvey['soil_0_15_Mn'],
      'Soil 0-15cm Zinc': fullSurvey['soil_0_15_Zn'],
      'Soil 0-15cm Iron': fullSurvey['soil_0_15_Fe'],
      'Soil 0-15cm Selenium': fullSurvey['soil_0_15_Se'],
      'Soil 0-15cm Chromium': fullSurvey['soil_0_15_Cr'],
      'Soil 0-15cm Lead': fullSurvey['soil_0_15_Pb'],
      'Soil 0-15cm Arsenic': fullSurvey['soil_0_15_As'],
      */
    'Carrot Polyphenols': fullSurvey[carrotKeys.Polyphenols],
    'Carrot Protein': fullSurvey[carrotKeys.Proteins],
    'Carrot Antioxidants': fullSurvey[carrotKeys.Antioxidants],
    /*
      'Carrot Phospohorus': carrotsOnly['food_P'],
      'Carrot Calcium': carrotsOnly['food_Ca'],
      'Carrot Copper': carrotsOnly['food_Cu'],
      'Carrot Magnesium': carrotsOnly['food_Mg'],
      'Carrot Manganese': carrotsOnly['food_Mn'],
      'Carrot Zinc': carrotsOnly['food_Zn'],
      'Carrot Iron': carrotsOnly['food_Fe'],
      'Carrot Selenium': carrotsOnly['food_Se'],
      'Carrot Chromium': carrotsOnly['food_Cr'],
      'Carrot Lead': carrotsOnly['food_Pb'],
      'Carrot Arsenic': carrotsOnly['food_As'],
      */
    'Spinach Polyphenols': fullSurvey[spinachKeys.Polyphenols],
    'Spinach Protein': fullSurvey[spinachKeys.Proteins],
    'Spinach Antioxidants': fullSurvey[spinachKeys.Antioxidants],
    /*
      'Spinach Phospohorus': spinachOnly['food_P'],
      'Spinach Calcium': spinachOnly['food_Ca'],
      'Spinach Copper': spinachOnly['food_Cu'],
      'Spinach Magnesium': spinachOnly['food_Mg'],
      'Spinach Manganese': spinachOnly['food_Mn'],
      'Spinach Zinc': spinachOnly['food_Zn'],
      'Spinach Iron': spinachOnly['food_Fe'],
      'Spinach Selenium': spinachOnly['food_Se'],
      'Spinach Chromium': spinachOnly['food_Cr'],
      'Spinach Lead': spinachOnly['food_Pb'],
      'Spinach Arsenic': spinachOnly['food_As'],
      */
  };

  //  console.log('carrots poly');
  //  console.log(carrots['veg_results.data.polyphenolsMgGae100gFw']);

  _.forEach(_.keys(showList), (k) => {
    const theArray = MathMore.numbersOnly(showList[k]);
    const n = theArray.length;
    //    console.log(theArray);
    //    dashboard.table({
    //        Average: `${MathMore.MathMEAN(theArray)}`,
    //        'Standard Dev': `${MathMore.MathSTDEV(theArray)}`,
    //        Min: `${MathMore.MathMIN(theArray)}`,
    //        Max: `${MathMore.MathMAX(theArray)}`,
    //      },
    //      `${k}, n = ${n}`,
    //    );
    dashboard.histogram(showList[k], `Histogram ${k}, n = ${n}`, '');
  });

  // don't have access to this data, so we can't really ask this question...
  //  const distinctSamples = utils.distinct(fullSurvey['group_farm.farm_name']);

  const saniteized = _.map(fullSurvey['store_group.store_name'], s => s
    .replace(/[^\w.]+/g, '')
    .toLowerCase()
    .replace(/^the/g, ''));

  const distinctStores = utils.distinct(saniteized);
  // dashboard.info(distinctStores.join('<br>'), 'Here are all the store names');

  const states = {
    CT: ['connecticut', 'CT'],
    NY: ['new york', 'NY'],
    MI: ['michigan', 'MI'],
    PA: ['pennsylvania', 'PA'],
    MA: ['ma'],
  };

  const stateStatistics = _.clone(states);
  const farmsByState = _.clone(states);
  const storesByState = _.clone(states);

  _.forEach(states, (v, k) => {
    console.log(`setting ${k}`);
    stateStatistics[k] = 0;
    farmsByState[k] = 0;
    storesByState[k] = 0;
  });

  _.forEach(fullSurvey.state, (state, idx) => {
    // console.log(`state: ${state}`);
    let found = false;
    _.forEach(states, (arr, short) => {
      if (_.find(arr, s => s === state.toLowerCase())) {
        stateStatistics[short] += 1;
        found = true;

        if (fullSurvey['store_group.store_name'][idx]) {
          storesByState[short] += 1;
        }

        if (fullSurvey['group_farm.farm_name'][idx]) {
          farmsByState[short] += 1;
        }
      }
    });

    if (!found) {
      if (!stateStatistics[state]) {
        stateStatistics[state] = 0;
      }
      stateStatistics[state] += 1;
    }
  });
  // ////////////////////////////////// CREATE MAIN CATEGORIES FOR COMPARISON ///////////////////////////////////////////

  // certified_organic, local, organic, biological_amendments, irrigation nonchlorine_water, notill, cover_crops, nospray, biodynamic, none
  // irrigate v non-irrigated

  const biological2 = [
    'certified_organic',
    'organic',
    'biological_amendments',
    'nonchlorine_water',
    'notill',
    'cover_crops',
    'nospray',
    'biodynamic',
  ];

  fullSurvey.bioScore = fullSurvey.metaInstanceID;
  for (let i = 0; i < fullSurvey.bioScore.length; i++) {
    fullSurvey.bioScore[i] = 0;
  }
  _.forEach(fullSurvey.farm_practice, (item, index) => {
    _.forEach(biological2, (trait, traitIndex) => {
      if (_.find(item.split(' '), p => p === biological2[traitIndex])) {
        fullSurvey.bioScore[index] += 1;
      }
    });
  });

  const irrigation = utils.filter(fullSurvey, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'scheduled_irrigation')) {
      return true;
    }
    return false;
  });
  const noBio = utils.filter(fullSurvey, (item) => {
    if (item.bioScore === 0) {
      return true;
    }
    return false;
  });
  const someBio = utils.filter(fullSurvey, (item) => {
    if (item.bioScore > 0 && item.bioScore < 4) {
      return true;
    }
    return false;
  });
  const lotsBio = utils.filter(fullSurvey, (item) => {
    if (item.bioScore >= 4) {
      return true;
    }
    return false;
  });
  const organic = utils.filter(fullSurvey, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
      return true;
    }
    return false;
  });
  /*
    const allOrganic = utils.filter(fullSurvey, (item) => {
      if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
        return true;
      }
      if (_.find(item.farm_practice.split(' '), p => p === 'organic')) {
        return true;
      }
      return false;
    });
    */
  const organicNotCert = utils.filter(fullSurvey, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
      return false;
    }
    if (_.find(item.farm_practice.split(' '), p => p === 'organic')) {
      return true;
    }
    return false;
  });

  /*
      console.log(irrigation);
      console.log(noBio);
      console.log(someBio);
      console.log(lotsBio);
      console.log(organic);
      console.log(allOrganic);
    */

  // ////////////////////////////////// FINAL DISPLAYS, GRAPHS, TABLES, ETC. ///////////////////////////////////////////

  // 'of farms and stores surveyed by state and count'
  stateMap('RFC Lab Surveys by state', stateStatistics, _.values(stateStatistics), 2, {
    lat: 44.716898999546984,
    lon: -81.55109146630394,
  });

  dashboard.table(
    {
      'Total surveys': fullSurvey.metaInstanceID.length,
      'Unique stores surveyed': distinctStores.length,
      //      'Farms surveyed': distinctSamples.length,
    },
    'RFC Lab Survey',
  );

  // dashboard.table(stateStatistics, 'Samples submitted by state');
  // can't do this because there's no way to count the number of separate farms... all the data is hidden!
  // dashboard.table(farmsByState, 'Surveyed samples from farms by state');
  /*
    dashboard.table(storesByState, 'Surveyed samples from stores by state');
    */

  console.log('total n');
  console.log(fullSurvey.metaInstanceID.length);
  console.log('irrigation n');
  console.log(irrigation.metaInstanceID.length);
  console.log('noBio n');
  console.log(noBio.metaInstanceID.length);
  console.log('someBio n');
  console.log(someBio.metaInstanceID.length);
  console.log('lotsBio n');
  console.log(lotsBio.metaInstanceID.length);
  console.log('organic n');
  console.log(organic.metaInstanceID.length);
  //  console.log('allOrganic n');
  //  console.log(allOrganic.metaInstanceID.length);
  console.log('OrganicNotCert n');
  console.log(organicNotCert.metaInstanceID.length);
  console.log(stateStatistics);

  dashboard.table(
    {
      irrigation: irrigation.metaInstanceID.length,
      noBio: noBio.metaInstanceID.length,
      someBio: someBio.metaInstanceID.length,
      lotsBio: lotsBio.metaInstanceID.length,
      organic: organic.metaInstanceID.length,
      //      allOrganic: allOrganic.metaInstanceID.length,
      organicNotCert: organicNotCert.metaInstanceID.length,
    },
    'Total surveys by Farm Practice',
  );

  dashboard.table(
    {
      irrigation:
        countIfNumber(irrigation[carrotKeys.Antioxidants])
        + countIfNumber(irrigation[spinachKeys.Antioxidants]),
      noBio:
        countIfNumber(noBio[carrotKeys.Antioxidants])
        + countIfNumber(noBio[spinachKeys.Antioxidants]),
      someBio:
        countIfNumber(someBio[carrotKeys.Antioxidants])
        + countIfNumber(someBio[spinachKeys.Antioxidants]),
      lotsBio:
        countIfNumber(lotsBio[carrotKeys.Antioxidants])
        + countIfNumber(lotsBio[spinachKeys.Antioxidants]),
      organic:
        countIfNumber(organic[carrotKeys.Antioxidants])
        + countIfNumber(organic[spinachKeys.Antioxidants]),
      //      allOrganic: countIfNumber(allOrganic[carrotKeys.Antioxidants]) +
      //        countIfNumber(allOrganic[spinachKeys.Antioxidants]),
      organicNotCert:
        countIfNumber(organicNotCert[carrotKeys.Antioxidants])
        + countIfNumber(organicNotCert[spinachKeys.Antioxidants]),
    },
    'Surveys with antioxidants by Farm Practice',
  );

  dashboard.table(
    {
      irrigation: countIfNumber(irrigation[soilKeys3['Total Carbon']]),
      noBio: countIfNumber(noBio[soilKeys3['Total Carbon']]),
      someBio: countIfNumber(someBio[soilKeys3['Total Carbon']]),
      lotsBio: countIfNumber(lotsBio[soilKeys3['Total Carbon']]),
      organic: countIfNumber(organic[soilKeys3['Total Carbon']]),
      //      'allOrganic': countIfNumber(allOrganic[soilKeys3['Total Carbon']]),
      organicNotCert: countIfNumber(organicNotCert[soilKeys3['Total Carbon']]),
    },
    'Surveys with Soil Respiration by Farm Practice (2 depths per survey)',
  );

  const irrigationCarrotsPoly = utils.filter(irrigation, numberFilter(carrotKeys.Polyphenols));
  const noBioCarrotsPoly = utils.filter(noBio, numberFilter(carrotKeys.Polyphenols));
  const someBioCarrotsPoly = utils.filter(someBio, numberFilter(carrotKeys.Polyphenols));
  const lotsBioCarrotsPoly = utils.filter(lotsBio, numberFilter(carrotKeys.Polyphenols));
  const organicCarrotsPoly = utils.filter(organic, numberFilter(carrotKeys.Polyphenols));
  //  const allOrganicCarrotsPoly = utils.filter(allOrganic, numberFilter(carrotKeys.Polyphenols));
  const organicNotCertCarrotsPoly = utils.filter(
    organicNotCert,
    numberFilter(carrotKeys.Polyphenols),
  );

  const irrigationCarrotsAnti = utils.filter(irrigation, numberFilter(carrotKeys.Antioxidants));
  const noBioCarrotsAnti = utils.filter(noBio, numberFilter(carrotKeys.Antioxidants));
  const someBioCarrotsAnti = utils.filter(someBio, numberFilter(carrotKeys.Antioxidants));
  const lotsBioCarrotsAnti = utils.filter(lotsBio, numberFilter(carrotKeys.Antioxidants));
  const organicCarrotsAnti = utils.filter(organic, numberFilter(carrotKeys.Antioxidants));
  //  const allOrganicCarrotsAnti = utils.filter(allOrganic, numberFilter(carrotKeys.Antioxidants));
  const organicNotCertCarrotsAnti = utils.filter(
    organicNotCert,
    numberFilter(carrotKeys.Antioxidants),
  );

  const irrigationCarrotsProt = utils.filter(irrigation, numberFilter(carrotKeys.Proteins));
  const noBioCarrotsProt = utils.filter(noBio, numberFilter(carrotKeys.Proteins));
  const someBioCarrotsProt = utils.filter(someBio, numberFilter(carrotKeys.Proteins));
  const lotsBioCarrotsProt = utils.filter(lotsBio, numberFilter(carrotKeys.Proteins));
  const organicCarrotsProt = utils.filter(organic, numberFilter(carrotKeys.Proteins));
  //  const allOrganicCarrotsProt = utils.filter(allOrganic, numberFilter(carrotKeys.Proteins));
  const organicNotCertCarrotsProt = utils.filter(organicNotCert, numberFilter(carrotKeys.Proteins));

  const irrigation3C = utils.filter(irrigation, numberFilter(soilKeys3['Total Carbon']));
  const noBio3C = utils.filter(noBio, numberFilter(soilKeys3['Total Carbon']));
  const someBio3C = utils.filter(someBio, numberFilter(soilKeys3['Total Carbon']));
  const lotsBio3C = utils.filter(lotsBio, numberFilter(soilKeys3['Total Carbon']));
  const organic3C = utils.filter(organic, numberFilter(soilKeys3['Total Carbon']));
  //  const allOrganic3C = utils.filter(allOrganic, numberFilter(soilKeys3['Total Carbon']));
  const organicNotCert3C = utils.filter(organicNotCert, numberFilter(soilKeys3['Total Carbon']));

  const irrigation3Resp = utils.filter(irrigation, numberFilter(soilKeys3.Respiration));
  const noBio3Resp = utils.filter(noBio, numberFilter(soilKeys3.Respiration));
  const someBio3Resp = utils.filter(someBio, numberFilter(soilKeys3.Respiration));
  const lotsBio3Resp = utils.filter(lotsBio, numberFilter(soilKeys3.Respiration));
  const organic3Resp = utils.filter(organic, numberFilter(soilKeys3.Respiration));
  //  const allOrganic3Resp = utils.filter(allOrganic, numberFilter(soilKeys3.Respiration));
  const organicNotCert3Resp = utils.filter(organicNotCert, numberFilter(soilKeys3.Respiration));

  const irrigation9C = utils.filter(irrigation, numberFilter(soilKeys9['Total Carbon']));
  const noBio9C = utils.filter(noBio, numberFilter(soilKeys9['Total Carbon']));
  const someBio9C = utils.filter(someBio, numberFilter(soilKeys9['Total Carbon']));
  const lotsBio9C = utils.filter(lotsBio, numberFilter(soilKeys9['Total Carbon']));
  const organic9C = utils.filter(organic, numberFilter(soilKeys9['Total Carbon']));
  //  const allOrganic9C = utils.filter(allOrganic, numberFilter(soilKeys9['Total Carbon']));
  const organicNotCert9C = utils.filter(organicNotCert, numberFilter(soilKeys9['Total Carbon']));

  const irrigation9Resp = utils.filter(irrigation, numberFilter(soilKeys9.Respiration));
  const noBio9Resp = utils.filter(noBio, numberFilter(soilKeys9.Respiration));
  const someBio9Resp = utils.filter(someBio, numberFilter(soilKeys9.Respiration));
  const lotsBio9Resp = utils.filter(lotsBio, numberFilter(soilKeys9.Respiration));
  const organic9Resp = utils.filter(organic, numberFilter(soilKeys9.Respiration));
  //  const allOrganic9Resp = utils.filter(allOrganic, numberFilter(soilKeys9.Respiration));
  const organicNotCert9Resp = utils.filter(organicNotCert, numberFilter(soilKeys9.Respiration));

  /*
      console.log(irrigation[soilKeys3['Total Carbon']]);
      console.log(irrigationC[soilKeys3['Total Carbon']]);
      console.log(irrigationC[soilKeys9['Total Carbon']]);

      console.log(noBio[soilKeys3['Total Carbon']]);
      console.log(noBioC[soilKeys3['Total Carbon']]);
      console.log(noBioC[soilKeys9['Total Carbon']]);

      console.log(someBio[soilKeys3['Total Carbon']]);
      console.log(someBioC[soilKeys3['Total Carbon']]);
      console.log(someBioC[soilKeys9['Total Carbon']]);

      console.log(lotsBio[soilKeys3['Total Carbon']]);
      console.log(lotsBioC[soilKeys3['Total Carbon']]);
      console.log(lotsBioC[soilKeys9['Total Carbon']]);

      console.log(organic[soilKeys3['Total Carbon']]);
      console.log(organicC[soilKeys3['Total Carbon']]);
      console.log(organicC[soilKeys9['Total Carbon']]);

      console.log(organicNotCert[soilKeys3['Total Carbon']]);
      console.log(organicNotCertC[soilKeys3['Total Carbon']]);
      console.log(organicNotCertC[soilKeys9['Total Carbon']]);
    */

  barchart(
    'Carrot Quality by farm practice',
    [
      `Polyphenols (n = ${countIfNumber(fullSurvey[carrotKeys.Polyphenols])})`,
      `Antioxidants (n = ${countIfNumber(fullSurvey[carrotKeys.Antioxidants])})`,
      `Proteins (n = ${countIfNumber(fullSurvey[carrotKeys.Proteins])})`,
    ],
    {
      Irrigation: [
        mean(irrigationCarrotsPoly, carrotKeys.Polyphenols),
        mean(irrigationCarrotsAnti, carrotKeys.Antioxidants),
        mean(irrigationCarrotsProt, carrotKeys.Proteins),
      ],
      noBio: [
        mean(noBioCarrotsPoly, carrotKeys.Polyphenols),
        mean(noBioCarrotsAnti, carrotKeys.Antioxidants),
        mean(noBioCarrotsProt, carrotKeys.Proteins),
      ],
      someBio: [
        mean(someBioCarrotsPoly, carrotKeys.Polyphenols),
        mean(someBioCarrotsAnti, carrotKeys.Antioxidants),
        mean(someBioCarrotsProt, carrotKeys.Proteins),
      ],
      lotsBio: [
        mean(lotsBioCarrotsPoly, carrotKeys.Polyphenols),
        mean(lotsBioCarrotsAnti, carrotKeys.Antioxidants),
        mean(lotsBioCarrotsProt, carrotKeys.Proteins),
      ],
      organic: [
        mean(organicCarrotsPoly, carrotKeys.Polyphenols),
        mean(organicCarrotsAnti, carrotKeys.Antioxidants),
        mean(organicCarrotsProt, carrotKeys.Proteins),
      ],
      //      allOrganic: [
      //        mean(allOrganicCarrotsPoly, carrotKeys.Polyphenols),
      //        mean(allOrganicCarrotsAnti, carrotKeys.Antioxidants),
      //        mean(allOrganicCarrotsProt, carrotKeys.Proteins),
      //      ],
      organicNotCert: [
        mean(organicNotCertCarrotsPoly, carrotKeys.Polyphenols),
        mean(organicNotCertCarrotsAnti, carrotKeys.Antioxidants),
        mean(organicNotCertCarrotsProt, carrotKeys.Proteins),
      ],
    },
  );

  /* // check for outliers in the data - check with stacked histogram, as well as with median rather than mean.
    barchart(
      'Carrot Quality by farm practice - MEDIAN',
      [
        `Polyphenols (n = ${countIfNumber(fullSurvey[carrotKeys.Polyphenols])})`,
        `Antioxidants (n = ${countIfNumber(fullSurvey[carrotKeys.Antioxidants])})`,
        `Proteins (n = ${countIfNumber(fullSurvey[carrotKeys.Proteins])})`,
      ], {
        Irrigation: [
          MathMore.MathMEDIAN(irrigationCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(irrigationCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(irrigationCarrotsProt[carrotKeys.Proteins]),
        ],
        noBio: [
          MathMore.MathMEDIAN(noBioCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(noBioCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(noBioCarrotsProt[carrotKeys.Proteins]),
        ],
        someBio: [
          MathMore.MathMEDIAN(someBioCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(someBioCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(someBioCarrotsProt[carrotKeys.Proteins]),
        ],
        lotsBio: [
          MathMore.MathMEDIAN(lotsBioCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(lotsBioCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(lotsBioCarrotsProt[carrotKeys.Proteins]),
        ],
        organic: [
          MathMore.MathMEDIAN(organicCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(organicCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(organicCarrotsProt[carrotKeys.Proteins]),
        ],
        allOrganic: [
          MathMore.MathMEDIAN(allOrganicCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(allOrganicCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(allOrganicCarrotsProt[carrotKeys.Proteins]),
        ],
        organicNotCert: [
          MathMore.MathMEDIAN(organicNotCertCarrotsPoly[carrotKeys.Polyphenols]),
          MathMore.MathMEDIAN(organicNotCertCarrotsAnti[carrotKeys.Antioxidants]),
          MathMore.MathMEDIAN(organicNotCertCarrotsProt[carrotKeys.Proteins]),
        ],
      },
    );

        */
  // //////////////////////// HISTOGRAMS - GOOD INFO BUT TOO MUCH TO SHOW /////////////////////

  /*
      let removeHigh = 300;
      let trace1 = {
        x: irrigationCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'irrigation',
        opacity: 0.5,
        marker: {
          color: 'green',
        },
      };
      let trace2 = {
        x: noBioCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'noBio',
        opacity: 0.5,
        marker: {
          color: 'red',
        },
      };
      let trace3 = {
        x: someBioCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'someBio',
        opacity: 0.5,
        marker: {
          color: 'blue',
        },
      };
      let trace4 = {
        x: lotsBioCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'lotsBio',
        opacity: 0.5,
        marker: {
          color: 'purple',
        },
      };
      let trace5 = {
        x: organicCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organic',
        opacity: 0.5,
        marker: {
          color: 'orange',
        },
      };
      /*
    //  let trace6 = {
    //    x: allOrganicCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
    //    type: 'histogram',
    //    name: 'allOrganic',
    //    opacity: 0.5,
    //    marker: {
    //      color: 'grey',
    //    },
    //  };

      let trace7 = {
        x: organicNotCertCarrotsAnti[carrotKeys.Antioxidants].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organicNotCert',
        opacity: 0.5,
        marker: {
          color: 'yellow',
        },
      };
      let data = [trace1, trace2, trace3, trace4, trace5, trace7];
      let layout = {
        barmode: 'stack'
      };
      let c = dashboard.card('Antioxidants, Histogram by group', '', {
        backgroundColor: '#fff',
        classes: 'col s12 m12',
      });
      Plotly.plot($(`#${c} .plot-container`)[0], data, layout);


      removeHigh = 999999;
      trace1 = {
        x: irrigationC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'irrigation',
        opacity: 0.5,
        marker: {
          color: 'green',
        },
      };
      trace2 = {
        x: noBioC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'noBio',
        opacity: 0.5,
        marker: {
          color: 'red',
        },
      };
      trace3 = {
        x: someBioC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'someBio',
        opacity: 0.5,
        marker: {
          color: 'blue',
        },
      };
      trace4 = {
        x: lotsBioC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'lotsBio',
        opacity: 0.5,
        marker: {
          color: 'purple',
        },
      };
      trace5 = {
        x: organicC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organic',
        opacity: 0.5,
        marker: {
          color: 'orange',
        },
      };

    //  trace6 = {
    //    x: allOrganicC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
    //    type: 'histogram',
    //    name: 'allOrganic',
    //    opacity: 0.5,
    //    marker: {
    //      color: 'grey',
    //    },
    //  };

      trace7 = {
        x: organicNotCertC[soilKeys3['Total Carbon']].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organicNotCert',
        opacity: 0.5,
        marker: {
          color: 'yellow',
        },
      };
      data = [trace1, trace2, trace3, trace4, trace5, trace7];
      layout = {
        barmode: 'stack'
      };
      c = dashboard.card('Total Carbon, Histogram by group', '', {
        backgroundColor: '#fff',
        classes: 'col s12 m12',
      });
      Plotly.plot($(`#${c} .plot-container`)[0], data, layout);


      removeHigh = 999999;
      trace1 = {
        x: irrigationResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'irrigation',
        opacity: 0.5,
        marker: {
          color: 'green',
        },
      };
      trace2 = {
        x: noBioResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'noBio',
        opacity: 0.5,
        marker: {
          color: 'red',
        },
      };
      trace3 = {
        x: someBioResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'someBio',
        opacity: 0.5,
        marker: {
          color: 'blue',
        },
      };
      trace4 = {
        x: lotsBioResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'lotsBio',
        opacity: 0.5,
        marker: {
          color: 'purple',
        },
      };
      trace5 = {
        x: organicResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organic',
        opacity: 0.5,
        marker: {
          color: 'orange',
        },
      };

    //  trace6 = {
    //    x: allOrganicResp[soilKeys3.Respiration].filter(value => value < removeHigh),
    //    type: 'histogram',
    //    name: 'allOrganic',
    //    opacity: 0.5,
    //    marker: {
    //      color: 'grey',
    //    },
    //  };

      trace7 = {
        x: organicNotCertResp[soilKeys3.Respiration].filter(value => value < removeHigh),
        type: 'histogram',
        name: 'organicNotCert',
        opacity: 0.5,
        marker: {
          color: 'yellow',
        },
      };
      data = [trace1, trace2, trace3, trace4, trace5, trace7];
      layout = {
        barmode: 'stack'
      };
      c = dashboard.card('Respiration, Histogram by group', '', {
        backgroundColor: '#fff',
        classes: 'col s12 m12',
      });
      Plotly.plot($(`#${c} .plot-container`)[0], data, layout);

    */

  //  SOILC
  const validCarrotSoilC = hasAllValuesAsNumber(fullSurvey, [
    carrotKeys.Antioxidants,
    soilKeys3['Total Carbon'],
    soilKeys9['Total Carbon'],
  ]);

  const validCarrotSoilResp = hasAllValuesAsNumber(fullSurvey, [
    carrotKeys.Antioxidants,
    soilKeys3.Respiration,
    soilKeys9.Respiration,
  ]);

  // notes
  // you almost always want to return the number of samples used or displayed in basically every function.  So should be accessible via the result.
  // for scatterplots, you want to know the shared (overlapping) n as well
  // function we need:
  // we should consider just wholesale copying structure and outcome as much as possible for R dplyr lib
  // https://dplyr.tidyverse.org/
  // copy existing column (right now this is ugly - must clone, and delete old)
  // copy existing row
  //
  //  mutate() adds new variables that are functions of existing variables <-- filter
  //  select() picks variables based on their names. <--
  //  filter() picks cases based on their values. <--
  //  summarise() reduces multiple values down to a single summary.
  //  arrange() changes the ordering of the rows.
  // rename columns
  // remove or reset specific entries based on criteria (>100 === nan etc.)

  console.log(carrotsOnly);

  /*
    _.forEach(sharedMinerals, (element) => {
      if (typeof carrotsOnly[`soil_0_15_${element}`] !== 'undefined' && typeof carrotsOnly[`food_${element}`] !== 'undefined') {
        //      if (hasAtLeastOneNumber(carrotsOnly[`soil_0_15_${element}`]) && hasAtLeastOneNumber(carrotsOnly[`food_${element}`])) {
        scatter(
          `Minerals in soil and food (n = ${getSharedN(
            carrotsOnly[`soil_0_15_${element}`],
            carrotsOnly[`food_${element}`],
          )})`,
          carrotsOnly[`soil_0_15_${element}`],
          carrotsOnly[`food_${element}`],
          [`${element} in carrot`, `${element} in soil`],
        );
      }
      //    }
    });
      */

  /*
      console.log(noBio);
      console.log(someBio);
      console.log(lotsBio);
      console.log(organic);
      console.log(allOrganic);
    */

  barchart(
    `Soil Carbon Changes by Depth (n = ${countIfNumber(fullSurvey[soilKeys3['Total Carbon']])})`,
    ['0 cm - 15 cm', '15 cm - 30 cm'],
    {
      Irrigation: [
        mean(irrigation3C, soilKeys3['Total Carbon']),
        mean(irrigation9C, soilKeys9['Total Carbon']),
      ],
      noBio: [mean(noBio3C, soilKeys3['Total Carbon']), mean(noBio9C, soilKeys9['Total Carbon'])],
      someBio: [
        mean(someBio3C, soilKeys3['Total Carbon']),
        mean(someBio9C, soilKeys9['Total Carbon']),
      ],
      lotsBio: [
        mean(lotsBio3C, soilKeys3['Total Carbon']),
        mean(lotsBio9C, soilKeys9['Total Carbon']),
      ],
      organic: [
        mean(organic3C, soilKeys3['Total Carbon']),
        mean(organic9C, soilKeys9['Total Carbon']),
      ],
      /*
        allOrganic: [
          mean(
            allOrganicC,
            soilKeys['Total Carbon'],
          ),
          mean(
            allOrganicC,
            soilKeys['Total Carbon'],
          ),
        ],
        */
      organicNotCert: [
        mean(organicNotCert3C, soilKeys3['Total Carbon']),
        mean(organicNotCert9C, soilKeys9['Total Carbon']),
      ],
    },
  );

  dashboard.table(
    {
      'irrigation n = ': countIfNumber(irrigation3C[soilKeys3['Total Carbon']]),
      'noBio n = ': countIfNumber(noBio3C[soilKeys3['Total Carbon']]),
      'someBio n = ': countIfNumber(someBio3C[soilKeys3['Total Carbon']]),
      'lotsBio n = ': countIfNumber(lotsBio3C[soilKeys3['Total Carbon']]),
      'organic n = ': countIfNumber(organic3C[soilKeys3['Total Carbon']]),
      //      'allOrganic n = ': countIfNumber(allOrganic3C[soilKeys3['Total Carbon']]),
      'organicNotCert n = ': countIfNumber(organicNotCert3C[soilKeys3['Total Carbon']]),
    },
    'Soil Carbon Changes by Depth',
  );

  barchart(
    `Soil Respiration Changes by Depth (n = ${countIfNumber(fullSurvey[soilKeys3.Respiration])})`,
    ['0 cm - 15 cm', '15 cm - 30 cm'],
    {
      Irrigation: [
        mean(irrigation3Resp, soilKeys3.Respiration),
        mean(irrigation9Resp, soilKeys9.Respiration),
      ],
      noBio: [mean(noBio3Resp, soilKeys3.Respiration), mean(noBio9Resp, soilKeys9.Respiration)],
      someBio: [
        mean(someBio3Resp, soilKeys3.Respiration),
        mean(someBio9Resp, soilKeys9.Respiration),
      ],
      lotsBio: [
        mean(lotsBio3Resp, soilKeys3.Respiration),
        mean(lotsBio9Resp, soilKeys9.Respiration),
      ],
      organic: [
        mean(organic3Resp, soilKeys3.Respiration),
        mean(organic9Resp, soilKeys9.Respiration),
      ],
      /*
        allOrganic: [
          mean(
            allOrganicResp,
            soilKeys3.Respiration,
          ),
          mean(
            allOrganicResp,
            soilKeys9.Respiration,
          ),
        ],
        */
      organicNotCert: [
        mean(organicNotCert3Resp, soilKeys3.Respiration),
        mean(organicNotCert9Resp, soilKeys9.Respiration),
      ],
    },
  );

  dashboard.table(
    {
      'irrigation n = ': countIfNumber(irrigation3Resp[soilKeys3['Total Carbon']]),
      'noBio n = ': countIfNumber(noBio3Resp[soilKeys3['Total Carbon']]),
      'someBio n = ': countIfNumber(someBio3Resp[soilKeys3['Total Carbon']]),
      'lotsBio n = ': countIfNumber(lotsBio3Resp[soilKeys3['Total Carbon']]),
      'organic n = ': countIfNumber(organic3Resp[soilKeys3['Total Carbon']]),
      //      'allOrganic n = ': countIfNumber(allOrganic3Resp[soilKeys3['Total Carbon']]),
      'organicNotCert n = ': countIfNumber(organicNotCert3Resp[soilKeys3['Total Carbon']]),
    },
    'Respiration Changes by Depth',
  );

  scatter(
    `Soil Carbon by nutrient density (Antioxidants) (n = ${getSharedN(
      validCarrotSoilC[soilKeys3['Total Carbon']],
      validCarrotSoilC[carrotKeys.Antioxidants],
    )})`,
    validCarrotSoilC[soilKeys3['Total Carbon']],
    validCarrotSoilC[carrotKeys.Antioxidants],
    ['Total Organic Carbon', 'Antioxidents'],
  );

  scatter(
    `Soil Respiration by nutrient density (Antioxidants) (n = ${getSharedN(
      validCarrotSoilResp[soilKeys3.Respiration],
      validCarrotSoilResp[carrotKeys.Antioxidants],
    )})`,
    validCarrotSoilResp[soilKeys3.Respiration],
    validCarrotSoilResp[carrotKeys.Antioxidants],
    ['Soil Respiration', 'Antioxidents'],
  );

  scatter(
    `Soil Carbon by nutrient density (Polyphenols) (n = ${getSharedN(
      validCarrotSoilC[soilKeys3['Total Carbon']],
      validCarrotSoilC[carrotKeys.Polyphenols],
    )})`,
    validCarrotSoilC[soilKeys3['Total Carbon']],
    validCarrotSoilC[carrotKeys.Polyphenols],
    ['Total Organic Carbon', 'Polyphenols'],
  );

  scatter(
    `Soil Respiration by nutrient density (Polyphenols) (n = ${getSharedN(
      validCarrotSoilResp[soilKeys3.Respiration],
      validCarrotSoilResp[carrotKeys.Polyphenols],
    )})`,
    validCarrotSoilResp[soilKeys3.Respiration],
    validCarrotSoilResp[carrotKeys.Polyphenols],
    ['Soil Respiration', 'Polyphenols'],
  );

  const requiredIndexes = [carrotKeys.Polyphenols, carrotKeys.Antioxidants];

  const validPolyAntiox = hasAllValuesAsNumber(fullSurvey, requiredIndexes);

  scatter(
    `Polyphenols (n = ${validPolyAntiox[carrotKeys.Polyphenols].length}) by Antioxidants (n = ${
      validPolyAntiox[carrotKeys.Antioxidants].length
    })`,
    validPolyAntiox[carrotKeys.Polyphenols],
    validPolyAntiox[carrotKeys.Antioxidants],
    ['Polyphenols', 'Antioxidants'],
  );

  const validSoil3RespTotalc = utils.filter(
    hasAllValuesAsNumber(fullSurvey, [soilKeys3['Total Carbon'], soilKeys3.Respiration]),
    item => Number.parseFloat(item['soil3.display_respiration.data.ugc_gsoil']) < 100,
  );

  const validSoil9RespTotalc = utils.filter(
    hasAllValuesAsNumber(fullSurvey, [soilKeys9['Total Carbon'], soilKeys9.Respiration]),
    item => Number.parseFloat(item['soil9.display_respiration.data.ugc_gsoil']) < 100,
  );

  scatter(
    `Soil Biological Activity 0 - 15cm, (n = ${
      validSoil3RespTotalc[soilKeys3['Total Carbon']].length
    }) by Total Organic Carbon, (n = ${validSoil3RespTotalc[soilKeys3.Respiration].length})`,
    validSoil3RespTotalc[soilKeys3['Total Carbon']],
    validSoil3RespTotalc[soilKeys3.Respiration],
    ['Total Organic Carbon', 'Soil Respiration'],
  );

  scatter(
    `Soil Biological Activity 15 - 30cm, (n = ${
      validSoil9RespTotalc[soilKeys9['Total Carbon']].length
    }) by Total Organic Carbon, (n = ${validSoil9RespTotalc[soilKeys9.Respiration].length})`,
    validSoil9RespTotalc[soilKeys9['Total Carbon']],
    validSoil9RespTotalc[soilKeys9.Respiration],
    ['Total Organic Carbon', 'Soil Respiration'],
  );

  const validCarrots = hasAtLeastOneNumber(fullSurvey, carrotscanAverageKeys);
  const traces = tracesFromKeys(validCarrots, carrotscanAverageKeys);

  const validSpinach = hasAtLeastOneNumber(fullSurvey, spinachscanAverageKeys);
  traces.push(tracesFromKeys(validSpinach, spinachscanAverageKeys));

  console.log(traces);
  linecharts('Spetral response carrot surface', _.keys(carrotscanAverageKeys), traces, [
    'wavelength [nm]',
    'signal',
  ]);

  const validCarrotSupernatants = hasAtLeastOneNumber(fullSurvey, carrotsSuperNatantKeys);
  const carrotsSuperNatantTraces = tracesFromKeys(validCarrotSupernatants, carrotsSuperNatantKeys);

  linecharts(
    `Spetral response, extracted carrot, (n = ${carrotsSuperNatantTraces.length})`,
    _.keys(carrotsSuperNatantKeys),
    carrotsSuperNatantTraces,
    ['wavelength [nm]', 'signal'],
  );

  const validSoil3Scan = hasAtLeastOneNumber(fullSurvey, soil3ScanVisKeys);
  const soil3ScanVisTraces = tracesFromKeys(validSoil3Scan, soil3ScanVisKeys);

  linecharts(
    `Spectral responses, Soil Quality 0 - 15cm, (n = ${soil3ScanVisTraces.length})`,
    _.keys(soil3ScanVisKeys),
    soil3ScanVisTraces,
    ['wavelength [nm]', 'signal'],
  );

  const validSoil9Scan = hasAtLeastOneNumber(fullSurvey, soil9ScanVisKeys);
  const soil9ScanVisTraces = tracesFromKeys(validSoil9Scan, soil9ScanVisKeys);

  linecharts(
    `Spectral responses, Soil Quality 15 - 30cm, (n = ${soil9ScanVisTraces.length})`,
    _.keys(soil9ScanVisKeys),
    soil9ScanVisTraces,
    ['wavelength [nm]', 'signal'],
  );

  dashboard.showLoading(false);
})();

/*
  ui.statBox({
    color: 'bg-green',
    title: 'CSV Merged',
    desc: `With Total ${joinedWithSpinach.metaInstanceID.length} entries`,
    icon: 'ion-stats-bars',
    action: 'Download CSV',
    actionIcon: 'fa-download',
    callback: () => {
      const payload = surveyToCSV(joinedWithSpinach);
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style = 'display: none';
      const blob = new Blob([payload], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = 'oursci-data.csv';
      a.click();
      window.URL.revokeObjectURL(url);
    },
  });
  */

/*
    ui.dataTable({
      id: 'all-joined',
      name: 'RFC data with spinach, carrots & soil',
      data: joinedWithSpinach,
    });

    */

/*
    ui.init({
      title: 'BFA Dashboard',
      desc: 'Dashboard for Lab Progression',
    });

    ui.info({
      title: 'first info box',
      description: 'and some content',
    });

    ui.info({
      title: 'second info box',
      description: 'and some content',
      icon: 'fa-plus',
    });

    return;

    */
