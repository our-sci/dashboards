/* global moment */
/* eslint-disable no-param-reassign */
/// <reference path="../.vscode/globals.d.ts" />
import { init } from 'https://unpkg.com/@oursci/dashboards@1/loader.js';
import * as utils from 'https://unpkg.com/@oursci/dashboards@1/utils.js';

import {
  linecharts,
  stateMap,
  activityGraph,
  histogram,
  countNumbers,
  barchart,
  numberFilter,
  mean,
  multiBarchart,
  getSharedN,
  hasAllValuesAsNumber,
  scatter,
  hasAtLeastOneNumber,
  tracesFromKeys,
  surveyActivity,
} from './functions.js';
import * as fixData from './fixdata.js';
import * as config from './fixconfig.js';

/**
 * Styling
 */
const style = {
  primary: '#ef4029',
  secondary: '#26abe3',
  accent: '#26abe3',
};

/**
 * list of surveys that are requried for the dashboard
 */
const surveys = {
  rfcv1: 'build_RFC-Sample-Collection-Survey_1529938938', // move store_group.store_additional into farm_practice keep existing values
  rfcv2: 'build_RFC-Sample-Collection-Survey-v2_1531946011',
  rfcv4: 'build_Sample-Collection-Survey-RFC_1531751577',
  rfcv3: 'build_Sample-Collection-Survey-RFC-v3_1532958979',
  soilv2: 'build_Soil-Quality-Survey-v2_1533147719',
  soilv3: 'build_Soil-Quality-Survey-v3_1536173868',
  carrotv1: 'build_Carrot-Quality-Survey-v1_1536173572',
  carrotv2: 'build_Carrot-Quality-Survey-A2_1531937323', // carrot scan raw + supernatant only, shift wavelengths. delete all else
  carrotv4: 'build_Carrot-Quality-Survey-A4_1532465603', // carrot scan raw + supernatant only, shift wavelengths.  delete all else
  carrotv5: 'build_Carrot-Quality-Survey-A5_1533131731', // shift wavelengths, note that supernatant was done on correct wavelengths but carrotscan needs to be shifted.
  carrotv6: 'build_Carrot-Quality-Survey-A6_1533563832',
  carrotv7: 'build_Carrot-Quality-Survey-A7_1534367514',
  spinachv1: 'build_Spinach-Quality-Survey-A1_1531938554', // leaf scan data only, shift wavelengths. delete all else
  spinachv2: 'build_Spinach-Quality-Survey-A2_1532541479', // supernatant only, shift wavelengths.  delete all else
  spinachv3: 'build_Spinach-Quality-Survey-A3_1533131824', // no leaf scan data, use supernatant, shift wavelengths.
  spinachv4: 'build_Spinach-Quality-Survey-A4_1534367478',
  spinachv5: 'build_Spinach-Quality-Survey-v2_1536173235',
};

/**
 * Render function called to build the dashboard
 */
function render(surveyData) {
  fixData.remapField(surveyData.rfcv1, 'store_group.store_additional', 'farm_practice');

  /**
   * Fix carrots
   */
  fixData.remapFields(surveyData.carrotv5, config.remapVegResults);
  fixData.remapFields(surveyData.carrotv6, config.remapVegResults);

  fixData.remapWavelengths(surveyData.carrotv1, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv1, config.carrotKeepList);

  fixData.remapWavelengths(surveyData.carrotv2, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv2, config.carrotKeepList);

  fixData.remapWavelengths(surveyData.carrotv4, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv4, config.carrotKeepList);

  fixData.remapWavelengths(surveyData.carrotv5, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv5, config.carrotKeepList);

  fixData.remapWavelengths(surveyData.carrotv6, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv6, config.carrotKeepList);

  fixData.remapWavelengths(surveyData.carrotv7, config.wavelengthRemap);
  fixData.removeFields(surveyData.carrotv7, config.carrotKeepList);

  /**
   * Fix spinach
   */

  // index.bfa.js says to replace the fields in spinachv4, yet they seem ok
  // fixData.remapFields(surveyData.spinachA4, config.remapVegResults);

  fixData.remapWavelengths(surveyData.spinachv1, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv1, config.keepSpinachLeafList);

  /**
   * Fix spinach supernatant
   */

  fixData.remapWavelengths(surveyData.spinachv1, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv1, config.keepSpinachLeafList);

  fixData.remapWavelengths(surveyData.spinachv2, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv2, config.keepSpinachSuperList);

  fixData.remapWavelengths(surveyData.spinachv3, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv3, config.keepSpinachLeafList);

  fixData.remapWavelengths(surveyData.spinachv4, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv4, config.keepSpinachLeafList);

  fixData.remapWavelengths(surveyData.spinachv5, config.wavelengthRemap);
  fixData.removeFields(surveyData.spinachv5, config.keepSpinachLeafList);

  /**
   * merge surveys
   */

  function merge(prefix) {
    return dashboard.mergeSurveys(_.values(_.filter(surveyData, (v, k) => k.startsWith(prefix))));
  }

  const rfc = merge('rfc');
  const spinach = merge('spinach');
  const carrots = merge('carrot');
  const soil = merge('soil');

  const spinachSan = fixData.sanetizeNumber(
    spinach,
    config.sanetizeVegConfig.Polyphenols,
    config.sanetizeVegConfig.Antioxidants,
    config.sanetizeVegConfig.Proteins,
  );

  const carrotSan = fixData.sanetizeNumber(
    carrots,
    config.sanetizeVegConfig.Polyphenols,
    config.sanetizeVegConfig.Antioxidants,
    config.sanetizeVegConfig.Proteins,
  );

  const soilSan = fixData.sanetizeNumber(
    soil,
    config.sanetizeSoilConfig['Total Carbon'],
    config.sanetizeSoilConfig.Respiration,
  );

  fixData.sanetizeNumber(carrots, ..._.values(config.carrotscan1Keys));
  fixData.sanetizeNumber(carrots, ..._.values(config.carrotscan1Keys));
  fixData.sanetizeNumber(carrots, ..._.values(config.superNatantKeys));

  fixData.sanetizeNumber(spinach, ..._.values(config.spinachscan1Keys));
  fixData.sanetizeNumber(spinach, ..._.values(config.spinachscan2Keys));
  fixData.sanetizeNumber(spinach, ..._.values(config.spinachscan3Keys));
  fixData.sanetizeNumber(spinach, ..._.values(config.superNatantKeys));

  const soilOutlierCallback = (data, idx) => {
    data['display_respiration.data.ugc_gsoil'][idx] = undefined;
  };

  const outlierstats = {};

  /**
   * Question: is it ok to remove the outliers in the array?
   */
  outlierstats.maxco2 = fixData.removeOutliers(
    soil,
    'display_respiration.data.Max_co2',
    null,
    10000,
    soilOutlierCallback,
  );
  outlierstats.minco2 = fixData.removeOutliers(
    soil,
    'display_respiration.data.Min_co2',
    null,
    10000,
    soilOutlierCallback,
  );
  outlierstats.ugc_gsoil = fixData.removeOutliers(
    soil,
    'display_respiration.data.ugc_gsoil',
    0,
    100,
    soilOutlierCallback,
  );
  outlierstats.co2increase = fixData.removeOutliers(
    soil,
    'display_respiration.data.co2_increase',
    null,
    10000,
    soilOutlierCallback,
  );

  outlierstats.totalc = fixData.removeOutliers(
    soil,
    config.sanetizeSoilConfig['Total Carbon'],
    0,
    100,
    (data, idx) => {
      console.log(`outlier detected: ${soil[config.sanetizeSoilConfig['Total Carbon'][idx]]}`);
    },
  );

  _.forEach(config.carrotscanAverageKeys, (v, k) => {
    fixData.averageColums(carrots, v, config.carrotscan1Keys[k], config.carrotscan2Keys[k]);
  });

  _.forEach(config.spinachscanAverageKeys, (v, k) => {
    fixData.averageColums(
      spinach,
      v,
      config.spinachscan1Keys[k],
      config.spinachscan2Keys[k],
      config.spinachscan3Keys[k],
    );
  });

  const soilQuality3 = utils.filter(soil, i => i.Depth === '3');
  const soilQuality9 = utils.filter(soil, i => i.Depth === '9');

  let joined = dashboard.joinSurveyOn(
    _.cloneDeep(rfc),
    'Sample_ID',
    soilQuality3,
    'sample_id',
    'soil3',
  );
  joined = dashboard.joinSurveyOn(joined, 'Sample_ID', soilQuality9, 'sample_id', 'soil9');
  joined = dashboard.joinSurveyOn(joined, 'Sample_ID', carrots, 'sample_id', 'carrots');
  joined = dashboard.joinSurveyOn(joined, 'Sample_ID', spinach, 'sample_id', 'spinach');

  /**
   * Surveys are joined
   */

  _.forEach(config.foodMinerals, (item) => {
    joined[`food_${item}`] = new Array(joined.metaInstanceID.length);
    _.fill(joined[`food_${item}`], undefined);
  });

  const soil3 = 'soil_0_15_';
  const soil9 = 'soil_15_30_';
  _.forEach(config.soilMinerals, (item) => {
    joined[`${soil3}${item}`] = new Array(joined.metaInstanceID.length);
    joined[`${soil9}${item}`] = new Array(joined.metaInstanceID.length);
    _.fill(joined[`${soil3}${item}`], undefined);
    _.fill(joined[`${soil9}${item}`], undefined);
  });

  // break out the minerals from array to individual variables for carrots

  const xrfSoil3 = fixData.extractXRF(joined, 'soil3', config.soilMinerals);
  _.forEach(xrfSoil3, (values, element) => {
    joined[`${soil3}${element}`] = values;
  });

  const xrfSoil9 = fixData.extractXRF(joined, 'soil9', config.soilMinerals);
  _.forEach(xrfSoil9, (values, element) => {
    joined[`${soil9}${element}`] = values;
  });

  const xrfCarrots = fixData.extractXRF(joined, 'carrots', config.foodMinerals);
  _.forEach(xrfCarrots, (values, element) => {
    joined[`food_${element}`] = values;
  });

  const xrfSpinach = fixData.extractXRF(joined, 'spinach', config.foodMinerals);
  _.forEach(xrfSpinach, (values, element) => {
    joined[`food_${element}`] = _.zipWith(joined[`food_${element}`], values, (a, b) => a || b);
  });

  const activity = surveyActivity(joined, 7);

  dashboard.section('Real Food Campaign Lab');
  dashboard.info(
    'Real Food Campaign Dashboard',
    `${_.values(rfc)[0].length} Surveys to date`,
    'fa-flask',
  );
  dashboard.info('Surveys processed past 7 days', `${activity.newSubmissions} surveys`, 'fa-calendar-alt');

  const states = {
    CT: ['connecticut', 'CT'],
    NY: ['new york', 'NY'],
    MI: ['michigan', 'MI'],
    PA: ['pennsylvania', 'PA'],
    MA: ['ma'],
  };

  const stateStatistics = fixData.countValues(rfc, 'state', states);

  const map = stateMap(
    'RFC Lab Surveys by state',
    stateStatistics,
    _.values(stateStatistics),
    2,
    {
      lat: 44.716898999546984,
      lon: -81.55109146630394,
    },
    style.accent,
  );

  dashboard.plotly('Samples by State', map.data, map.layout, map.config, true);

  const saniteized = _.map(joined['store_group.store_name'], s => s
    .replace(/[^\w.]+/g, '')
    .toLowerCase()
    .replace(/^the/g, ''));

  const distinctStores = utils.distinct(saniteized);

  dashboard.info('Stores surveyed', distinctStores.length, 'fa-store-alt');

  dashboard.section('Activity');

  const activityPlot = activityGraph(activity.activity);
  const measurementActivityPlot = activityGraph(activity.measurementActivity);

  dashboard.plotly(
    'Survey Submission',
    activityPlot.data,
    activityPlot.layout,
    activityPlot.config,
  );

  dashboard.plotly(
    'Measurement Activity',
    measurementActivityPlot.data,
    measurementActivityPlot.layout,
    measurementActivityPlot.config,
  );

  dashboard.section('Sampling Progress');

  dashboard.progressBox(
    'Spinach Progress',
    `Polyphenols / Antioxidants / Proteins (ongoing: ${spinachSan.nan} measurements)`,
    spinachSan.completeness,
    spinachSan.completenessStr,
    'fa-apple-alt',
  );

  dashboard.progressBox(
    'Carrots Progress',
    `Polyphenols / Antioxidants / Proteins (ongoing: ${carrotSan.nan} measurements)`,
    carrotSan.completeness,
    carrotSan.completenessStr,
    'fa-apple-alt',
  );

  dashboard.progressBox(
    'Soil Progress',
    `Total C / Respiration (ongoing: ${soilSan.nan} measurements)`,
    soilSan.completeness,
    soilSan.completenessStr,
    'fa-atom',
  );

  dashboard.section('Survey Data');

  dashboard.csvDownloadBox('Combined Survey', joined);

  dashboard.section('Partial Surveys');

  dashboard.csvDownloadBox('RFC Base Survey', rfc);
  dashboard.csvDownloadBox('Carrot Survey', carrots);
  dashboard.csvDownloadBox('Spinach Survey', spinach);
  dashboard.csvDownloadBox('Soil Survey', soil);
  dashboard.csvDownloadBox('Soil Quality 0 - 15 cm', soilQuality3);
  dashboard.csvDownloadBox('Soil Quality 15 cm - 30 cm', soilQuality9);

  dashboard.section('Quality of Data');

  dashboard.progressBox(
    'Max CO2',
    'Data Outlier Integrity',
    outlierstats.maxco2.integrity,
    outlierstats.maxco2.integrityStr,
    'fa-chart-pie',
  );

  dashboard.progressBox(
    'Min CO2',
    'Outlier Integrity',
    outlierstats.minco2.integrity,
    outlierstats.minco2.integrityStr,
    'fa-chart-pie',
  );

  dashboard.progressBox(
    'Soil Respiration',
    'Outlier Integrity',
    outlierstats.ugc_gsoil.integrity,
    outlierstats.ugc_gsoil.integrityStr,
    'fa-chart-pie',
  );

  dashboard.progressBox(
    'Total C',
    'Outlier Integrity',
    outlierstats.totalc.integrity,
    outlierstats.totalc.integrityStr,
    'fa-chart-pie',
  );

  dashboard.section('Distribution Summary of Spinach, Carrots and Soil');

  console.log(joined);
  const histograms = {
    'Soil Respiration 0 - 15cm': config.keys.soil3.resp,
    'Total Organic Carbon 0 - 15cm': config.keys.soil3.carbon,
    'Soil Respiration 15 - 30cm': config.keys.soil9.resp,
    'Total Organic Carbon 15 - 30cm': config.keys.soil9.resp,
    /*
      'Soil 0-15cm Phospohorus': 'soil_0_15_P',
      'Soil 0-15cm Calcium': 'soil_0_15_Ca',
      'Soil 0-15cm Copper': 'soil_0_15_Cu',
      'Soil 0-15cm Magnesium': 'soil_0_15_Mg',
      'Soil 0-15cm Manganese': 'soil_0_15_Mn',
      'Soil 0-15cm Zinc': 'soil_0_15_Zn',
      'Soil 0-15cm Iron': 'soil_0_15_Fe',
      'Soil 0-15cm Selenium': 'soil_0_15_Se',
      'Soil 0-15cm Chromium': 'soil_0_15_Cr',
      'Soil 0-15cm Lead': 'soil_0_15_Pb',
      'Soil 0-15cm Arsenic': 'soil_0_15_As',
      */
    'Carrot Polyphenols': config.keys.carrots.polyphenols,
    'Carrot Protein': config.keys.carrots.proteins,
    'Carrot Antioxidants': config.keys.carrots.antioxidants,
    /*
      'Carrot Phospohorus': carrotsOnly['food_P'],
      'Carrot Calcium': carrotsOnly['food_Ca'],
      'Carrot Copper': carrotsOnly['food_Cu'],
      'Carrot Magnesium': carrotsOnly['food_Mg'],
      'Carrot Manganese': carrotsOnly['food_Mn'],
      'Carrot Zinc': carrotsOnly['food_Zn'],
      'Carrot Iron': carrotsOnly['food_Fe'],
      'Carrot Selenium': carrotsOnly['food_Se'],
      'Carrot Chromium': carrotsOnly['food_Cr'],
      'Carrot Lead': carrotsOnly['food_Pb'],
      'Carrot Arsenic': carrotsOnly['food_As'],
      */
    'Spinach Polyphenols': config.keys.spinach.polyphenols,
    'Spinach Protein': config.keys.spinach.proteins,
    'Spinach Antioxidants': config.keys.spinach.antioxidants,
    /*
      'Spinach Phospohorus': spinachOnly['food_P'],
      'Spinach Calcium': spinachOnly['food_Ca'],
      'Spinach Copper': spinachOnly['food_Cu'],
      'Spinach Magnesium': spinachOnly['food_Mg'],
      'Spinach Manganese': spinachOnly['food_Mn'],
      'Spinach Zinc': spinachOnly['food_Zn'],
      'Spinach Iron': spinachOnly['food_Fe'],
      'Spinach Selenium': spinachOnly['food_Se'],
      'Spinach Chromium': spinachOnly['food_Cr'],
      'Spinach Lead': spinachOnly['food_Pb'],
      'Spinach Arsenic': spinachOnly['food_As'],
      */
  };

  _.forEach(histograms, (v, k) => {
    const r = fixData.sanetizeNumber(joined, v);

    const plotlyData = histogram(joined[v], 50);
    dashboard.plotly(
      `${k} (N = ${r.count - r.nan})`,
      plotlyData.data,
      plotlyData.layout,
      plotlyData.config,
    );
  });

  dashboard.section('Qualification');

  dashboard.table({
    'Total Organic Carbon': '% carbon in soil (dried @ 105C)',
    'Soil Respiration': 'ug mineralizable carbon per gram soil (air dried)',
    Antioxidants: 'uM Gallic Acid equivalents (FRAP method), dry weight',
    Polyphenols: 'ug/ml Gallic Acid equivalents (Folin Ciocalteu method), dry weight',
    Proteins: 'mg protein per 100g dry weight',
  });

  const legend = {
    "'biological'":
      'each of the following items gives +1 to biological score: certified organic, organic, uses biological amendments, non-chlroinated irrigation water, no till, uses cover crops, no spray, biodynamic',
    Irrigation: "'frequently used irrigation' checked",
    noBio: "nothing in 'biological' list checked",
    someBio: "1 - 2 'biological' checked",
    lotsBio: "3 or more 'biological' checked",
    organic: "'certified organic' checked",
    organicNotCert: "'organic' checked, 'certified organic' not checked",
  };
  dashboard.table(legend);

  //  CREATE MAIN CATEGORIES FOR COMPARISON

  // certified_organic, local, organic, biological_amendments,
  // irrigation nonchlorine_water, notill, cover_crops, nospray, biodynamic, none
  // irrigate v non-irrigated

  const biological2 = [
    'certified_organic',
    'organic',
    'biological_amendments',
    'nonchlorine_water',
    'notill',
    'cover_crops',
    'nospray',
    'biodynamic',
  ];

  joined.bioScore = joined.metaInstanceID;
  for (let i = 0; i < joined.bioScore.length; i++) {
    joined.bioScore[i] = 0;
  }
  _.forEach(joined.farm_practice, (item, index) => {
    _.forEach(biological2, (trait, traitIndex) => {
      if (_.find(item.split(' '), p => p === biological2[traitIndex])) {
        joined.bioScore[index] += 1;
      }
    });
  });

  const irrigation = utils.filter(joined, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'scheduled_irrigation')) {
      return true;
    }
    return false;
  });
  const noBio = utils.filter(joined, (item) => {
    if (item.bioScore === 0) {
      return true;
    }
    return false;
  });
  const someBio = utils.filter(joined, (item) => {
    if (item.bioScore > 0 && item.bioScore < 4) {
      return true;
    }
    return false;
  });
  const lotsBio = utils.filter(joined, (item) => {
    if (item.bioScore >= 4) {
      return true;
    }
    return false;
  });
  const organic = utils.filter(joined, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
      return true;
    }
    return false;
  });
  /*
    const allOrganic = utils.filter(joined, (item) => {
      if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
        return true;
      }
      if (_.find(item.farm_practice.split(' '), p => p === 'organic')) {
        return true;
      }
      return false;
    });
    */
  const organicNotCert = utils.filter(joined, (item) => {
    if (_.find(item.farm_practice.split(' '), p => p === 'certified_organic')) {
      return false;
    }
    if (_.find(item.farm_practice.split(' '), p => p === 'organic')) {
      return true;
    }
    return false;
  });

  dashboard.section('Farm practice summary');
  const practiceLabel = {
    irrigation: "'frequently used irrigation' checked",
    noBio: "nothing in 'biological' list checked",
    someBio: "1 - 2 'biological' checked",
    lotsBio: "3 or more 'biological' checked",
    organic: "'certified organic' checked",
    organicNotCert: "'organic' checked, 'certified organic' not checked",
  };
  const practiceStats = {
    irrigation: irrigation.metaInstanceID.length,
    noBio: noBio.metaInstanceID.length,
    someBio: someBio.metaInstanceID.length,
    lotsBio: lotsBio.metaInstanceID.length,
    organic: organic.metaInstanceID.length,
    //      allOrganic: allOrganic.metaInstanceID.length,
    organicNotCert: organicNotCert.metaInstanceID.length,
  };
  const practiceStatsPlot = barchart(practiceStats, practiceLabel);
  dashboard.plotly(
    'Total surveys by Farm Practice',
    practiceStatsPlot.data,
    practiceStatsPlot.layout,
    practiceStatsPlot.conifg,
  );

  dashboard.section('Surveys with antioxidants by Farm Practice');
  const antioxidantsStats = {
    irrigation:
      countNumbers(irrigation[config.keys.carrots.antioxidants])
      + countNumbers(irrigation[config.keys.spinach.antioxidants]),
    noBio:
      countNumbers(noBio[config.keys.carrots.antioxidants])
      + countNumbers(noBio[config.keys.spinach.antioxidants]),
    someBio:
      countNumbers(someBio[config.keys.carrots.antioxidants])
      + countNumbers(someBio[config.keys.spinach.antioxidants]),
    lotsBio:
      countNumbers(lotsBio[config.keys.carrots.antioxidants])
      + countNumbers(lotsBio[config.keys.spinach.antioxidants]),
    organic:
      countNumbers(organic[config.keys.carrots.antioxidants])
      + countNumbers(organic[config.keys.spinach.antioxidants]),
    //      allOrganic: countNumbers(allOrganic[config.keys.carrots.antioxidants]) +
    //        countNumbers(allOrganic[config.keys.spinach.antioxidants]),
    organicNotCert:
      countNumbers(organicNotCert[config.keys.carrots.antioxidants])
      + countNumbers(organicNotCert[config.keys.spinach.antioxidants]),
  };

  const antioxidantsStatsPlot = barchart(antioxidantsStats, practiceLabel);
  dashboard.plotly(
    'Surveys with antioxidants by Farm Practice',
    antioxidantsStatsPlot.data,
    antioxidantsStatsPlot.layout,
    antioxidantsStatsPlot.conifg,
  );

  dashboard.section('Surveys with Soil Respiration by Farm Practice (2 depths per survey)');
  const soilStats = {
    irrigation: countNumbers(irrigation[config.keys.soil3.carbon]),
    noBio: countNumbers(noBio[config.keys.soil3.carbon]),
    someBio: countNumbers(someBio[config.keys.soil3.carbon]),
    lotsBio: countNumbers(lotsBio[config.keys.soil3.carbon]),
    organic: countNumbers(organic[config.keys.soil3.carbon]),
    //      'allOrganic': countNumbers(allOrganic[`soil.${config.soilScanVisKeys}`]),
    organicNotCert: countNumbers(organicNotCert[config.keys.soil3.carbon]),
  };
  const soilStatsPlot = barchart(soilStats, practiceLabel);
  dashboard.plotly(
    'Surveys with Soil Respiration by Farm Practice',
    soilStatsPlot.data,
    soilStatsPlot.layout,
    soilStatsPlot.conifg,
  );

  const irrigationCarrotsPoly = utils.filter(
    irrigation,
    numberFilter(config.keys.carrots.polyphenols),
  );
  const noBioCarrotsPoly = utils.filter(noBio, numberFilter(config.keys.carrots.polyphenols));
  const someBioCarrotsPoly = utils.filter(
    someBio,
    numberFilter(config.keys.carrots.polyphenols),
  );
  const lotsBioCarrotsPoly = utils.filter(
    lotsBio,
    numberFilter(config.keys.carrots.polyphenols),
  );
  const organicCarrotsPoly = utils.filter(
    organic,
    numberFilter(config.keys.carrots.polyphenols),
  );
  // const allOrganicCarrotsPoly = utils.filter(allOrganic,
  // numberFilter(config.keys.carrots.polyphenols));

  const organicNotCertCarrotsPoly = utils.filter(
    organicNotCert,
    numberFilter(config.keys.carrots.polyphenols),
  );

  const irrigationCarrotsAnti = utils.filter(
    irrigation,
    numberFilter(config.keys.carrots.antioxidants),
  );
  const noBioCarrotsAnti = utils.filter(noBio, numberFilter(config.keys.carrots.antioxidants));
  const someBioCarrotsAnti = utils.filter(
    someBio,
    numberFilter(config.keys.carrots.antioxidants),
  );
  const lotsBioCarrotsAnti = utils.filter(
    lotsBio,
    numberFilter(config.keys.carrots.antioxidants),
  );
  const organicCarrotsAnti = utils.filter(
    organic,
    numberFilter(config.keys.carrots.antioxidants),
  );

  // const allOrganicCarrotsAnti = utils.filter(allOrganic,
  // numberFilter(config.keys.carrots.antioxidants));

  const organicNotCertCarrotsAnti = utils.filter(
    organicNotCert,
    numberFilter(config.keys.carrots.antioxidants),
  );

  const irrigationCarrotsProt = utils.filter(
    irrigation,
    numberFilter(config.keys.carrots.proteins),
  );
  const noBioCarrotsProt = utils.filter(noBio, numberFilter(config.keys.carrots.proteins));
  const someBioCarrotsProt = utils.filter(someBio, numberFilter(config.keys.carrots.proteins));
  const lotsBioCarrotsProt = utils.filter(lotsBio, numberFilter(config.keys.carrots.proteins));
  const organicCarrotsProt = utils.filter(organic, numberFilter(config.keys.carrots.proteins));

  //  const allOrganicCarrotsProt = utils.filter(allOrganic,
  // numberFilter(config.keys.carrots.proteins));

  const organicNotCertCarrotsProt = utils.filter(
    organicNotCert,
    numberFilter(config.keys.carrots.proteins),
  );

  const irrigation3C = utils.filter(irrigation, numberFilter(config.keys.soil3.carbon));
  const noBio3C = utils.filter(noBio, numberFilter(config.keys.soil3.carbon));
  const someBio3C = utils.filter(someBio, numberFilter(config.keys.soil3.carbon));
  const lotsBio3C = utils.filter(lotsBio, numberFilter(config.keys.soil3.carbon));
  const organic3C = utils.filter(organic, numberFilter(config.keys.soil3.carbon));

  // const allOrganic3C = utils.filter(allOrganic,
  // numberFilter(config.keys.soil3.carbon));

  const organicNotCert3C = utils.filter(organicNotCert, numberFilter(config.keys.soil3.carbon));

  const irrigation3Resp = utils.filter(irrigation, numberFilter(config.keys.soil3.resp));
  const noBio3Resp = utils.filter(noBio, numberFilter(config.keys.soil3.resp));
  const someBio3Resp = utils.filter(someBio, numberFilter(config.keys.soil3.resp));
  const lotsBio3Resp = utils.filter(lotsBio, numberFilter(config.keys.soil3.resp));
  const organic3Resp = utils.filter(organic, numberFilter(config.keys.soil3.resp));
  //  const allOrganic3Resp = utils.filter(allOrganic,
  // numberFilter(`soil3.${config.sanetizeSoilConfig['Respiration']}`));
  const organicNotCert3Resp = utils.filter(organicNotCert, numberFilter(config.keys.soil3.resp));

  const irrigation9C = utils.filter(irrigation, numberFilter(config.keys.soil9.carbon));
  const noBio9C = utils.filter(noBio, numberFilter(config.keys.soil9.carbon));
  const someBio9C = utils.filter(someBio, numberFilter(config.keys.soil9.carbon));
  const lotsBio9C = utils.filter(lotsBio, numberFilter(config.keys.soil9.carbon));
  const organic9C = utils.filter(organic, numberFilter(config.keys.soil9.carbon));
  //  const allOrganic9C = utils.filter(allOrganic,
  // numberFilter(config.keys.soil9.resp));
  const organicNotCert9C = utils.filter(organicNotCert, numberFilter(config.keys.soil9.carbon));

  const irrigation9Resp = utils.filter(irrigation, numberFilter(config.keys.soil9.resp));
  const noBio9Resp = utils.filter(noBio, numberFilter(config.keys.soil9.resp));
  const someBio9Resp = utils.filter(someBio, numberFilter(config.keys.soil9.resp));
  const lotsBio9Resp = utils.filter(lotsBio, numberFilter(config.keys.soil9.resp));
  const organic9Resp = utils.filter(organic, numberFilter(config.keys.soil9.resp));
  //  const allOrganic9Resp = utils.filter(allOrganic,
  // numberFilter(`soil9.${config.sanetizeSoilConfig['Respiration']}`));
  const organicNotCert9Resp = utils.filter(organicNotCert, numberFilter(config.keys.soil9.resp));

  dashboard.section('Carrot Quality');

  const carrotQualityPlot = multiBarchart(
    [
      `Polyphenols (n = ${countNumbers(joined[config.keys.carrots.polyphenols])})`,
      `Antioxidants (n = ${countNumbers(joined[config.keys.carrots.antioxidants])})`,
      `Proteins (n = ${countNumbers(joined[config.keys.carrots.proteins])})`,
    ],
    {
      Irrigation: [
        mean(irrigationCarrotsPoly, config.keys.carrots.polyphenols),
        mean(irrigationCarrotsAnti, config.keys.carrots.antioxidants),
        mean(irrigationCarrotsProt, config.keys.carrots.proteins),
      ],
      noBio: [
        mean(noBioCarrotsPoly, config.keys.carrots.polyphenols),
        mean(noBioCarrotsAnti, config.keys.carrots.antioxidants),
        mean(noBioCarrotsProt, config.keys.carrots.proteins),
      ],
      someBio: [
        mean(someBioCarrotsPoly, config.keys.carrots.polyphenols),
        mean(someBioCarrotsAnti, config.keys.carrots.antioxidants),
        mean(someBioCarrotsProt, config.keys.carrots.proteins),
      ],
      lotsBio: [
        mean(lotsBioCarrotsPoly, config.keys.carrots.polyphenols),
        mean(lotsBioCarrotsAnti, config.keys.carrots.antioxidants),
        mean(lotsBioCarrotsProt, config.keys.carrots.proteins),
      ],
      organic: [
        mean(organicCarrotsPoly, config.keys.carrots.polyphenols),
        mean(organicCarrotsAnti, config.keys.carrots.antioxidants),
        mean(organicCarrotsProt, config.keys.carrots.proteins),
      ],
      //      allOrganic: [
      //        mean(allOrganicCarrotsPoly, config.keys.carrots.polyphenols),
      //        mean(allOrganicCarrotsAnti, config.keys.carrots.antioxidants),
      //        mean(allOrganicCarrotsProt, carrotKeys.Proteins),
      //      ],
      organicNotCert: [
        mean(organicNotCertCarrotsPoly, config.keys.carrots.polyphenols),
        mean(organicNotCertCarrotsAnti, config.keys.carrots.antioxidants),
        mean(organicNotCertCarrotsProt, config.keys.carrots.proteins),
      ],
    },
  );

  dashboard.plotly(
    'Carrot Quality by farm practice',
    carrotQualityPlot.data,
    carrotQualityPlot.layout,
    carrotQualityPlot.config,
    true,
  );

  dashboard.section('Soil Carbon Changes by Depth');

  const soilQualityPlot = multiBarchart(['0 cm - 15 cm', '15 cm - 30 cm'], {
    Irrigation: [
      mean(irrigation3C, config.keys.soil3.carbon),
      mean(irrigation9C, config.keys.soil9.carbon),
    ],
    noBio: [mean(noBio3C, config.keys.soil3.carbon), mean(noBio9C, config.keys.soil9.carbon)],
    someBio: [
      mean(someBio3C, config.keys.soil3.carbon),
      mean(someBio9C, config.keys.soil9.carbon),
    ],
    lotsBio: [
      mean(lotsBio3C, config.keys.soil3.carbon),
      mean(lotsBio9C, config.keys.soil9.carbon),
    ],
    organic: [
      mean(organic3C, config.keys.soil3.carbon),
      mean(organic9C, config.keys.soil9.carbon),
    ],
    /*
        allOrganic: [
          mean(
            allOrganicC,
            soilKeys['Total Carbon'],
          ),
          mean(
            allOrganicC,
            soilKeys['Total Carbon'],
          ),
        ],
        */
    organicNotCert: [
      mean(organicNotCert3C, config.keys.soil3.carbon),
      mean(organicNotCert9C, config.keys.soil9.carbon),
    ],
  });

  dashboard.plotly(
    `Soil Carbon Changes by Depth (n = ${countNumbers(joined[config.keys.soil3.carbon])})`,
    soilQualityPlot.data,
    soilQualityPlot.layout,
    soilQualityPlot.config,
    true,
  );

  dashboard.table({
    'irrigation n = ': countNumbers(irrigation3C[config.keys.soil3.carbon]),
    'noBio n = ': countNumbers(noBio3C[config.keys.soil3.carbon]),
    'someBio n = ': countNumbers(someBio3C[config.keys.soil3.carbon]),
    'lotsBio n = ': countNumbers(lotsBio3C[config.keys.soil3.carbon]),
    'organic n = ': countNumbers(organic3C[config.keys.soil3.carbon]),
    //      'allOrganic n = ': countNumbers(allOrganic3C[config.keys.soil3.carbon]),
    'organicNotCert n = ': countNumbers(organicNotCert3C[config.keys.soil3.carbon]),
  });

  dashboard.section('Soil Respiration by Depth');

  const soilRespirationPlot = multiBarchart(['0 cm - 15 cm', '15 cm - 30 cm'], {
    Irrigation: [
      mean(irrigation3Resp, config.keys.soil3.resp),
      mean(irrigation9Resp, config.keys.soil9.resp),
    ],
    noBio: [
      mean(noBio3Resp, config.keys.soil3.resp),
      mean(noBio9Resp, config.keys.soil9.resp),
    ],
    someBio: [
      mean(someBio3Resp, config.keys.soil3.resp),
      mean(someBio9Resp, config.keys.soil9.resp),
    ],
    lotsBio: [
      mean(lotsBio3Resp, config.keys.soil3.resp),
      mean(lotsBio9Resp, config.keys.soil9.resp),
    ],
    organic: [
      mean(organic3Resp, config.keys.soil3.resp),
      mean(organic9Resp, config.keys.soil9.resp),
    ],
    /*
        allOrganic: [
          mean(
            allOrganicC,
            soilKeys.Respiration,
          ),
          mean(
            allOrganicC,
            soilKeys.Respiration,
          ),
        ],
        */
    organicNotCert: [
      mean(organicNotCert3Resp, config.keys.soil3.resp),
      mean(organicNotCert9Resp, config.keys.soil9.resp),
    ],
  });

  dashboard.plotly(
    `Soil Respiration Changes by Depth (n = ${countNumbers(joined[config.keys.soil3.resp])})`,
    soilRespirationPlot.data,
    soilRespirationPlot.layout,
    soilRespirationPlot.config,
    true,
  );

  dashboard.table({
    'irrigation n = ': countNumbers(irrigation3Resp[config.keys.soil3.resp]),
    'noBio n = ': countNumbers(noBio3Resp[config.keys.soil3.resp]),
    'someBio n = ': countNumbers(someBio3Resp[config.keys.soil3.resp]),
    'lotsBio n = ': countNumbers(lotsBio3Resp[config.keys.soil3.resp]),
    'organic n = ': countNumbers(organic3Resp[config.keys.soil3.resp]),
    //      'allOrganic n = ': countNumbers(allOrganic3C[config.keys.soil3.resp]),
    'organicNotCert n = ': countNumbers(organicNotCert3Resp[config.keys.soil3.resp]),
  });

  dashboard.section('Nutrient - Soil Correlation');

  const rfcScatter = (title, x, y, titleX, titleY) => {
    const validNumbers = hasAllValuesAsNumber(joined, [x, y]);

    const scatterPlot = scatter(validNumbers[x], validNumbers[y], [titleX, titleY]);

    dashboard.plotly(
      `${title} (n = ${getSharedN(validNumbers[x], validNumbers[y])})`,
      scatterPlot.data,
      scatterPlot.layout,
      scatterPlot.config,
      true,
    );
  };

  rfcScatter(
    'Soil Carbon by nutrient density (Antioxidants)',
    config.keys.soil3.carbon,
    config.keys.carrots.antioxidants,
    'Total Organic Carbon',
    'Antioxidents',
  );

  rfcScatter(
    'Soil Respiration by nutrient density (Antioxidants)',
    config.keys.soil3.resp,
    config.keys.carrots.antioxidants,
    'Soil Respiration',
    'Antioxidents',
  );

  rfcScatter(
    'Soil Carbon by nutrient density (Polyphenols)',
    config.keys.soil3.carbon,
    config.keys.carrots.polyphenols,
    'Total Organic Carbon',
    'Polyphenols',
  );

  rfcScatter(
    'Soil Respiration by nutrient density (Polyphenols)',
    config.keys.soil3.resp,
    config.keys.carrots.polyphenols,
    'Soil Respiration',
    'Polyphenols',
  );

  rfcScatter(
    'Polyphenols by Antioxidants',
    config.keys.carrots.polyphenols,
    config.keys.carrots.antioxidants,
    'Polyphenols',
    'Antioxidants',
  );

  rfcScatter(
    'Soil Biological Activity 0 - 15cm by Total Organic Carbon',
    config.keys.soil3.carbon,
    config.keys.soil3.resp,
    'Total Organic Carbon',
    'Soil Respiration',
  );

  rfcScatter(
    'Soil Biological Activity 15 - 30cm by Total Organic Carbon',
    config.keys.soil9.carbon,
    config.keys.soil9.resp,
    'Total Organic Carbon',
    'Soil Respiration',
  );

  dashboard.section('Spectral Information');

  const waveLengthPlot = (title, yaxisLabel, prefix, keys) => {
    const avgKeys = _.map(keys, v => `${prefix}.${v}`);
    const validSet = hasAtLeastOneNumber(joined, avgKeys);
    const trace = tracesFromKeys(validSet, avgKeys, 'Sample_ID');
    const p = linecharts(_.keys(avgKeys), trace.traces, trace.names, [
      'wavelength [nm]',
      yaxisLabel,
    ]);

    dashboard.plotly(`${title} (n = ${trace.traces.length})`, p.data, p.layout, p.config);
  };

  // carrots surface

  waveLengthPlot(
    'Spectral response carrot surface',
    'signal',
    'carrots',
    config.carrotscanAverageKeys,
  );

  waveLengthPlot(
    'Spectral response, extracted carrot',
    'signal',
    'carrots',
    config.superNatantKeys,
  );

  waveLengthPlot(
    'Spectral responses, Soil Quality 0 - 15cm',
    'signal',
    'soil3',
    config.soilScanVisKeys,
  );

  waveLengthPlot(
    'Spectral responses, Soil Quality 15 - 30cm',
    'signal',
    'soil9',
    config.soilScanVisKeys,
  );

  // console.log(outlierstats);

  // dashboard.debug('Combined Survey', joined);
  // dashboard.debug('RFC Base Survey', rfc);
  // dashboard.debug('Carrot Survey', carrots);
  // dashboard.debug('Spinach Survey', spinach);
  // dashboard.debug('Soil Survey', soil);
}
init({
  surveys,
  style,
  render,
});
